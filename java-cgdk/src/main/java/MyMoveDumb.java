import java.util.List;
import java.util.Set;

import model.Car;
import model.Game;
import model.Move;
import model.World;

public class MyMoveDumb {

	public static void makeDumbMove(Car self, World world, Game game, Set<List<MyTile>> pathes, Move move,
			boolean isBackMove) {
		MyVector direction = new MyVector(Math.cos(self.getAngle()), Math.sin(self.getAngle()));

		List<MyTile> bestPath = null;
		double maxDot = 0;
		for (List<MyTile> candidate : pathes) {
			double angular = MyMoveDumb.getVectorToMove(self, candidate, world, game).getAngle();
			MyVector toMove = new MyVector(Math.cos(angular), Math.sin(angular));
			double dot = toMove.dotProduct(direction);
			if (bestPath == null || dot > maxDot) {
				bestPath = candidate;
				maxDot = dot;
			}
		}

		MyVector vectorToMove = MyMoveDumb.getVectorToMove(self, bestPath, world, game);
		double angularToMove = vectorToMove.getAngle();

		MyVector velocity = new MyVector(self.getSpeedX(), self.getSpeedY());
		double scalarMulVel = velocity.dotProduct(direction);

		boolean isSameDirectionVel = true;
		if (scalarMulVel < 0 && velocity.getLen() > 0.1)
			isSameDirectionVel = false;

		double angularMoveCar = self.getAngle() - angularToMove;
		if (angularMoveCar < -Math.PI)
			angularMoveCar += 2 * Math.PI;
		if (angularMoveCar > Math.PI)
			angularMoveCar -= 2 * Math.PI;

		boolean isTurnRight = angularMoveCar > 0;

		if (isBackMove) {
			move.setBrake(false);
			move.setEnginePower(-1.0);
		} else {
			if (isBrakeNeeded(vectorToMove, velocity, self.getAngularSpeed(), game, self, bestPath, world))
				move.setBrake(true);
			move.setEnginePower(1.0);
		}

		if (isSameDirectionVel && Math.abs(Math.sin(angularMoveCar)*vectorToMove.getLen()) < 60) {
			move.setWheelTurn(0.0);
		} else if (!isBackMove) {
			if (isSameDirectionVel ^ isTurnRight) {
				move.setWheelTurn(1.0);
			} else {
				move.setWheelTurn(-1.0);
			}
		} else {
			if (isTurnRight) {
				move.setWheelTurn(1.0);
			} else {
				move.setWheelTurn(-1.0);
			}
		}

	}

	private static boolean isBrakeNeeded(MyVector vectorToMove, MyVector velocity, double angularSpeed,
			Game game, Car self, List<MyTile> bestPath , World world) {
		double lenVel = velocity.getLen();
		if (lenVel < 11)
			return false;
		double alfa = MyMoveTrajectoryModeler.normalizeAngle(vectorToMove.getAngle() - velocity.getAngle());
		double lenToMove = vectorToMove.getLen();
		if (Math.abs(Math.sin(alfa) * lenToMove) < 100)
			return false;
		double radiusCar;
		if (Math.abs(angularSpeed) < 0.00001 || self.getWheelTurn() < 11.5/lenVel){
			radiusCar =(lenVel/11.5) / game.getCarAngularSpeedFactor();//TODO: check
		}else{
			radiusCar = Math.abs(lenVel / angularSpeed);
		}

		// 
//		assert radiusCar >= 1 / game.getCarAngularSpeedFactor() : "radius: " + radiusCar + " Velocity: " + lenVel + " angularSpeed: " + angularSpeed;
		
		if (lenToMove < 2 * radiusCar){
			if(Math.abs(alfa) > Math.PI/2)
				return true;
		
			double betta = Math.asin(lenToMove / (2 * radiusCar));
			if (Math.abs(alfa) > betta)
				return true;
		}
		
		//TODO: check path has not collision
		
		MyVector raius = new MyVector(radiusCar, 0);
		MyVector radiusPerpendInDirectionToMove;
		if(alfa>0) //TODO: check
			radiusPerpendInDirectionToMove = raius.rotate(velocity.getAngle() + Math.PI/2);
		else
			radiusPerpendInDirectionToMove = raius.rotate(velocity.getAngle() - Math.PI/2);
		
		MyVector radiusPerpendInDirectionVsMove = radiusPerpendInDirectionToMove.multiply(-1);
		MyVector radiusToCheck = radiusPerpendInDirectionVsMove.rotate(alfa);
		MyVector pointToCheck = new MyVector(self.getX(), self.getY()).add(radiusPerpendInDirectionToMove).add(radiusToCheck);
		MyTile tileWithPoint = MyTile.getTile(pointToCheck.x, pointToCheck.y);
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());
		if(tileWithPoint == null || !tileWithPoint.equals(selfTile) && !tileWithPoint.equals(bestPath.get(0)))
			return true;
		
		if(MyWorldSimulator.isCollision(world, game, new MyWorldSimulator.Point(pointToCheck.x, pointToCheck.y)))
			return true;
		//TODO: next point to move

		MyVector toMoveNext;
		if(bestPath.size()>2){
			toMoveNext = getVectorToMove(world, game, bestPath.get(1), bestPath.get(2), self.getX() + vectorToMove.x, self.getY() + vectorToMove.y);
		}else{
			int changeX = bestPath.get(1).x - bestPath.get(0).x;
			int changeY = bestPath.get(1).y - bestPath.get(0).y;
			toMoveNext = new MyVector(changeX*game.getTrackTileSize(), changeY*game.getTrackTileSize());
		}
		
		double angleToMoveNext = MyMoveTrajectoryModeler.normalizeAngle(toMoveNext.getAngle() - vectorToMove.getAngle());
		double angleNextMoveFromNextVel = Math.abs(angleToMoveNext) - Math.abs(alfa);
		if(angleNextMoveFromNextVel < 0)
			return false;
		
		double nextBetta = Math.asin(toMoveNext.getLen() / (2 * radiusCar));
		if (angleNextMoveFromNextVel > nextBetta)
			return true;

		return false;
	}

	private static MyVector getVectorToMove(Car self, List<MyTile> path, World world, Game game) {
		MyTile nextTile = path.get(0);
		MyTile afterNextTile = path.get(1);
		double selfX = self.getX();
		double selfY = self.getY();
		return getVectorToMove(world, game, nextTile, afterNextTile, selfX, selfY);
	}

	private static MyVector getVectorToMove(World world, Game game, MyTile nextTile, MyTile afterNextTile,
			double selfX, double selfY) {
		MyTile current = MyTile.getTile(selfX, selfY);
		assert nextTile.getConnectedTiles(world).contains(afterNextTile);

		double centerCurrentX = current.x * game.getTrackTileSize() + game.getTrackTileSize() / 2;
		double centerCurrentY = current.y * game.getTrackTileSize() + game.getTrackTileSize() / 2;
		double centerNextX = nextTile.x * game.getTrackTileSize() + game.getTrackTileSize() / 2;
		double centerNextY = nextTile.y * game.getTrackTileSize() + game.getTrackTileSize() / 2;
		double centerAfterX = afterNextTile.x * game.getTrackTileSize() + game.getTrackTileSize() / 2;
		double centerAfterY = afterNextTile.y * game.getTrackTileSize() + game.getTrackTileSize() / 2;

//		double y = (7 * centerNextY + 3 * centerAfterY) / 10;
//		double x = (7 * centerNextX + 3 * centerAfterX) / 10;
		
		double x = 0.4 * centerNextX + 0.3*centerCurrentX + 0.3*centerAfterX;
		double y = 0.4 * centerNextY + 0.3*centerCurrentY + 0.3*centerAfterY;
		
		return new MyVector(x - selfX, y - selfY);
	}

}
