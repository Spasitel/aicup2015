import java.util.List;
import java.util.Set;

import model.Car;
import model.CarType;
import model.Game;
import model.World;


public class MyEvaluationPath {

	public static int getEstimationFromPath(Car nextState, Set<List<MyTile>> pathes, Game game, World world) {
		int result = -1;
		for (List<MyTile> path : pathes) {
			int resForPath = MyEvaluationPath.getEstimation(nextState, path, game, world);
			if (result < 0 || resForPath < result)
				result = resForPath;
		}
		return result;
	}

	private static int getEstimation(Car nextState, List<MyTile> path, Game game, World world) {
		MyTile lastTile = path.get(path.size() - 1);
		MyTile carTile = MyTile.getTile(nextState.getX(), nextState.getY());
		if (carTile.equals(lastTile))
			return 0;
	
		double distanceToMove = MyEvaluationPath.getEstimateDistance(nextState, path, game, world, lastTile);
		if (distanceToMove < 0)
			return 0;
	
		double timeToMove = MyEvaluationPath.getEstimateTime(nextState, distanceToMove, game, world);
	
		return (int) timeToMove;
	}

	private static double getEstimateDistance(Car nextState, List<MyTile> path, Game game, World world, MyTile lastTile) {
		double trackTileSize = game.getTrackTileSize();
		double halfTileSize = trackTileSize / 2;
		double finishX = lastTile.x * trackTileSize + halfTileSize;
		double finishY = lastTile.y * trackTileSize + halfTileSize;
		Double distanceToCenter = new MyVector(finishX - nextState.getX(), finishY - nextState.getY()).getLen();
		if (MyEvaluationPath.removeDistance < 0)
			MyEvaluationPath.removeDistance = new MyVector(halfTileSize, halfTileSize - game.getTrackTileMargin()).getLen();
	
		double distanceToMove = distanceToCenter - MyEvaluationPath.removeDistance;
	
		double distanceFromPath = MyEvaluationPath.getDistanceFromPath(nextState, path, game, world);
		distanceToMove = Math.max(distanceToMove, distanceFromPath);
		return distanceToMove;
	}

	private static double getDistanceFromPath(Car nextState, List<MyTile> nextTiles, Game game, World world) {
		MyTile curTile = MyTile.getTile(nextState.getX(), nextState.getY());
		double result = 0;
		int startIndex;
		if (nextTiles.indexOf(curTile) < 0) {
			MyTile start = nextTiles.get(0);
			if (!curTile.equals(start.getBot()) && !curTile.equals(start.getTop()) && !curTile.equals(start.getRight())
					&& !curTile.equals(start.getLeft())) {
				return 1000000;// we go in bad direction TODO:
			}
			startIndex = 1;
			result += MyEvaluationPath.getDistanceToTile(nextState, curTile, start, world, game);
			result += MyEvaluationPath.getDistanceInTile(curTile, start, nextTiles.get(1), world, game);
		} else {
			startIndex = nextTiles.indexOf(curTile) + 1;
			result += MyEvaluationPath.getDistanceToTile(nextState, curTile, nextTiles.get(startIndex), world, game);
		}
	
		for (int i = startIndex; i < nextTiles.size() - 2; i++) {
			result += MyEvaluationPath.getDistanceInTile(nextTiles.get(i - 1), nextTiles.get(i), nextTiles.get(i + 1), world, game);
		}
	
		return result;
	}

	private static double getDistanceInTile(MyTile prevTile, MyTile curTile, MyTile nextTile, World world, Game game) {
		if (prevTile.equals(nextTile))
			return game.getTrackTileSize();
		if (curTile.getBot() != null && curTile.getTop() != null) {
			if (curTile.getBot().equals(prevTile) && curTile.getTop().equals(nextTile)
					|| curTile.getBot().equals(nextTile) && curTile.getTop().equals(prevTile))
				return game.getTrackTileSize();
		}
		if (curTile.getRight() != null && curTile.getLeft() != null) {
			if (curTile.getRight().equals(prevTile) && curTile.getLeft().equals(nextTile)
					|| curTile.getRight().equals(nextTile) && curTile.getLeft().equals(prevTile))
				return game.getTrackTileSize();
		}
		return 2 * game.getTrackTileMargin();
	}

	private static double getDistanceToTile(Car nextState, MyTile curTile, MyTile nextTile, World world, Game game) {
		if (curTile.getBot() != null && curTile.getBot().equals(nextTile)) {
			double result = nextTile.y * game.getTrackTileSize() - nextState.getY();
			assert result > 0 && result < game.getTrackTileSize();
			return result;
		}
		if (curTile.getTop() != null && curTile.getTop().equals(nextTile)) {
			double result = -curTile.y * game.getTrackTileSize() + nextState.getY();
			assert result > 0 && result < game.getTrackTileSize();
			return result;
		}
		if (curTile.getLeft() != null && curTile.getLeft().equals(nextTile)) {
			double result = -curTile.x * game.getTrackTileSize() + nextState.getX();
			assert result > 0 && result < game.getTrackTileSize();
			return result;
		}
		if (curTile.getRight() != null && curTile.getRight().equals(nextTile)) {
			double result = nextTile.x * game.getTrackTileSize() - nextState.getX();
			assert result > 0 && result < game.getTrackTileSize();
			return result;
		}
		assert false;
		return 100000;
	}

	private static double removeDistance = -1;

	private static double getEstimateTime(Car nextState, double distanceToMove, Game game, World world) {
		int ticks = 0;
		double moveTraveled = 0;
	
		double currentSpeed = Math.sqrt(nextState.getSpeedX() * nextState.getSpeedX() + nextState.getSpeedY()
				* nextState.getSpeedY());
	
		double maxAcceleration;
	
		if (nextState.getType().equals(CarType.BUGGY))
			maxAcceleration = game.getBuggyEngineForwardPower() / game.getBuggyMass();
		else
			maxAcceleration = game.getJeepEngineForwardPower() / game.getJeepMass();
	
		while (moveTraveled < distanceToMove) {
			ticks++;
			double accelerationEngine;
			if (nextState.getRemainingNitroTicks() > ticks)
				accelerationEngine = maxAcceleration * 2;
			else
				accelerationEngine = maxAcceleration;
	
			double accelerationFromAir = currentSpeed * game.getCarMovementAirFrictionFactor();
			double totalAcceleration = accelerationEngine - accelerationFromAir;
	
			currentSpeed += totalAcceleration;
			moveTraveled += currentSpeed;
		}
	
		// double maxAcceleration = 0.25;// TODO: from game or world
		// double timeToMove = (Math.sqrt(currentSpeed * currentSpeed + 2 * distanceToMove * maxAcceleration) - currentSpeed)
		// / maxAcceleration;// TODO: is it true?
		return ticks - 1;
	}

}
