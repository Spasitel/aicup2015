import java.util.LinkedList;
import java.util.List;

import model.Car;
import model.Game;
import model.TileType;
import model.World;

public class MyWorldSimulator {

	private static final double minDistance = 10.0;
	
	public static boolean isCollision(World world, Game game,double xCenter, double yCenter, double angle) {

		// First check angles of car
		List<Point> corners = getCorners(game, xCenter, yCenter, angle);

		return isCollisionInCorners(world, game, corners);
	}

	public static boolean isCollision(Car car, World world, Game game) {

		// First check angles of car
		List<Point> corners = getCorners(car, world, game);

		return isCollisionInCorners(world, game, corners);
	}

	private static boolean isCollisionInCorners(World world, Game game, List<Point> corners) {
		for (Point corner : corners) {
			if (isCollision(world, game, corner))
				return true;
		}
		// TODO: not only for corners
		return false;
	}

	public static boolean isCollision(World world, Game game, Point corner) {
		TileType type = getTileTypeForPoint(corner, world, game);
		boolean collisionInTile = isCollisionInTile(type, corner, game);
		return collisionInTile;
	}

	//OUT - only corner, IN - corner between borders

	private static boolean isCollisionInTile(TileType type, Point corner, Game game) {
		
		double trackTileSize = game.getTrackTileSize();
		double x = corner.getX()%trackTileSize;
		double y = corner.getY()%trackTileSize;

		switch (type) {

		case BOTTOM_HEADED_T:
			return isCollisionBorderTop(x,y,game) 
					|| isCollisionOutCornerBotLeft(x,y,game)
					|| isCollisionOutCornerBotRight(x, y, game);
		case LEFT_HEADED_T:
			return isCollisionBorderRight(x,y,game) 
					|| isCollisionOutCornerBotLeft(x,y,game)
					|| isCollisionOutCornerTopLeft(x, y, game);
		case RIGHT_HEADED_T:
			return isCollisionBorderLeft(x,y,game) 
					|| isCollisionOutCornerTopRight(x,y,game)
					|| isCollisionOutCornerBotRight(x, y, game);
		case TOP_HEADED_T:
			return isCollisionBorderBot(x,y,game) 
					|| isCollisionOutCornerTopLeft(x,y,game)
					|| isCollisionOutCornerTopRight(x, y, game);

		case LEFT_BOTTOM_CORNER:
			return 
					isCollisionOutCornerTopRight(x, y, game) 
					|| isCollisionBorderBot(x, y, game)
					|| isCollisionBorderLeft(x, y, game)
					|| isCollisionInCornerBotLeft(x, y, game)
					;
		case LEFT_TOP_CORNER:
			return 
					isCollisionOutCornerBotRight(x, y, game) 
					|| isCollisionBorderTop(x, y, game)
					|| isCollisionBorderLeft(x, y, game)
					|| isCollisionInCornerTopLeft(x, y, game)
					;
		case RIGHT_BOTTOM_CORNER:
			return 
					isCollisionOutCornerTopLeft(x, y, game) 
					|| isCollisionBorderBot(x, y, game)
					|| isCollisionBorderRight(x, y, game)
					|| isCollisionInCornerBotRight(x, y, game)
					;
		case RIGHT_TOP_CORNER:
			return 
					isCollisionOutCornerBotLeft(x, y, game) 
					|| isCollisionBorderTop(x, y, game)
					|| isCollisionBorderRight(x, y, game)
					|| isCollisionInCornerTopRight(x, y, game)
					;

		case VERTICAL:
			return 
					isCollisionBorderRight(x, y, game) 
					|| isCollisionBorderLeft(x, y, game)
					;
		case HORIZONTAL:
			return 
					isCollisionBorderTop(x, y, game) 
					|| isCollisionBorderBot(x, y, game)
					;

		case CROSSROADS:
			return 
					isCollisionOutCornerTopLeft(x, y, game) 
					|| isCollisionOutCornerBotLeft(x,y,game)
					|| isCollisionOutCornerBotRight(x, y, game)
					|| isCollisionOutCornerTopRight(x, y, game)
					;
		case EMPTY:
			return true;
		case UNKNOWN:
			return true;
		default:
			assert false;
			return true;
		
		}
	}
	
	/*
	 * ****************************IN*****************
	 */

	public static boolean isCollisionInCorner(Game game, double a, double b) {
		double trackTileMargin = game.getTrackTileMargin();
		if(a > trackTileMargin*2)
			return false;
		if(b > trackTileMargin*2)
			return false;
		if( (trackTileMargin*2-a)*(trackTileMargin*2-a) + (trackTileMargin*2-b)*(trackTileMargin*2-b) < (trackTileMargin-minDistance)*(trackTileMargin-minDistance))
			return false;
		
		return true;
	}

	private static boolean isCollisionInCornerBotLeft(double x, double y, Game game) {
		double trackTileSize = game.getTrackTileSize();
		double a = x;
		double b = trackTileSize - y;
		
		return isCollisionInCorner(game, a, b);
	}


	private static boolean isCollisionInCornerTopLeft(double x, double y, Game game) {
		double a = x;
		double b = y;
		
		return isCollisionInCorner(game, a, b);
	}

	private static boolean isCollisionInCornerBotRight(double x, double y, Game game) {
		double trackTileSize = game.getTrackTileSize();
		double a = trackTileSize - x;
		double b = trackTileSize - y;
		
		return isCollisionInCorner(game, a, b);
	}

	private static boolean isCollisionInCornerTopRight(double x, double y, Game game) {
		double trackTileSize = game.getTrackTileSize();
		double a = trackTileSize - x;
		double b = y;
		
		return isCollisionInCorner(game, a, b);
	}
	
	/*
	 * ****************************OUT*****************
	 */

	public static boolean isCollisionOutCorner(Game game, double a, double b) {
		double trackTileMargin = game.getTrackTileMargin();
		if(a > trackTileMargin)
			return false;
		if(b > trackTileMargin)
			return false;
		if( (a)*(a) + (b)*(b) > (trackTileMargin+minDistance)*(trackTileMargin+minDistance))
			return false;
		
		return true;
	}
	
	private static boolean isCollisionOutCornerBotLeft(double x, double y, Game game) {
		double trackTileSize = game.getTrackTileSize();
		double a = x;
		double b = trackTileSize - y;
		
		return isCollisionOutCorner(game, a, b);
	}

	private static boolean isCollisionOutCornerTopLeft(double x, double y, Game game) {
		double a = x;
		double b = y;
		
		return isCollisionOutCorner(game, a, b);
	}

	private static boolean isCollisionOutCornerBotRight(double x, double y, Game game) {
		double trackTileSize = game.getTrackTileSize();
		double a = trackTileSize - x;
		double b = trackTileSize - y;
		
		return isCollisionOutCorner(game, a, b);
	}

	private static boolean isCollisionOutCornerTopRight(double x, double y, Game game) {
		double trackTileSize = game.getTrackTileSize();
		double a = trackTileSize - x;
		double b = y;
		
		return isCollisionOutCorner(game, a, b);
	}

	/*
	 * ****************************BORDER*****************
	 */
	
	private static boolean isCollisionBorderTop(double x, double y, Game game) {
		double trackTileMargin = game.getTrackTileMargin();
		if (y < trackTileMargin+minDistance)
			return true;
		return false;
	}

	private static boolean isCollisionBorderBot(double x, double y, Game game) {
		double trackTileMargin = game.getTrackTileMargin();
		double trackTileSize = game.getTrackTileSize();
		if (trackTileSize - y < trackTileMargin+minDistance)
			return true;
		return false;
	}
	private static boolean isCollisionBorderLeft(double x, double y, Game game) {
		double trackTileMargin = game.getTrackTileMargin();
		if (x < trackTileMargin+minDistance)
			return true;
		return false;
	}
	private static boolean isCollisionBorderRight(double x, double y, Game game) {
		double trackTileMargin = game.getTrackTileMargin();
		double trackTileSize = game.getTrackTileSize();
		if (trackTileSize - x < trackTileMargin+minDistance)
			return true;
		return false;
	}
	
	
	private static TileType getTileTypeForPoint(Point corner, World world, Game game) {
		MyTile currentTile = MyTile.getTile(corner.getX(), corner.getY());
		return currentTile.getType(world);
	}

	private static List<Point> getCorners(Car car, World world, Game game) {
		double xCenter = car.getX();
		double yCenter = car.getY();

		double angle = car.getAngle();

		return getCorners(game, xCenter, yCenter, angle);
	}

	private static List<Point> getCorners(Game game, double xCenter, double yCenter, double angle) {
		double deltaX = game.getCarWidth() / 2;
		double deltaY = game.getCarHeight() / 2;

		double shiftX1 = deltaX * Math.cos(angle) + deltaY * Math.sin(angle);
		double shiftY1 = deltaX * Math.sin(angle) - deltaY * Math.cos(angle);

		double shiftX2 = deltaX * Math.cos(angle) - deltaY * Math.sin(angle);
		double shiftY2 = deltaX * Math.sin(angle) + deltaY * Math.cos(angle);

		double shiftX3 = -shiftX1;
		double shiftY3 = -shiftY1;

		double shiftX4 = -shiftX2;
		double shiftY4 = -shiftY2;

		Point corner1 = new Point(xCenter + shiftX1, yCenter + shiftY1);
		Point corner2 = new Point(xCenter + shiftX2, yCenter + shiftY2);
		Point corner3 = new Point(xCenter + shiftX3, yCenter + shiftY3);
		Point corner4 = new Point(xCenter + shiftX4, yCenter + shiftY4);

		List<Point> result = new LinkedList<MyWorldSimulator.Point>();
		result.add(corner1);
		result.add(corner2);
		result.add(corner3);
		result.add(corner4);
		return result;
	}

	static class Point {
		private final double x;
		private final double y;

		public Point(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}

		public double getX() {
			return x;
		}

		public double getY() {
			return y;
		}

	}
}
