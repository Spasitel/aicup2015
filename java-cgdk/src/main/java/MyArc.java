import static java.lang.Math.*;

public class MyArc implements MyGeometryObject {

	public final MyVector center;
	public final double radius, angle1, angle2;
	public final MyTile tile;

	public MyArc(MyVector center, double radius, double angle1, double angle2, MyTile tile) {
		this.center = center;
		this.radius = radius;
		this.angle1 = angle1;
		this.angle2 = angle2;
		this.tile = tile;
	}

	public MyArc(double x, double y, double radius, double angle1, double angle2, MyTile tile) {
		this(new MyVector(x, y), radius, angle1, angle2, tile);
	}

	@Override
	public boolean intersect(MySegment other) {
		return intersectSegment((MySegment) other);
	}

	public boolean intersectSegment(MySegment oldSegment) {

		MyVector maxVector = getMaxVector();
		MyVector minVector = getMinVector();
		double maxX = maxVector.x;
		double minX = minVector.x;
		double maxY = maxVector.y;
		double minY = minVector.y;

		if (oldSegment.a.x > maxX && oldSegment.b.x > maxX)
			return false;

		if (oldSegment.a.x < minX && oldSegment.b.x < minX)
			return false;
		if (oldSegment.a.y > maxY && oldSegment.b.y > maxY)
			return false;
		if (oldSegment.a.y < minY && oldSegment.b.y < minY)
			return false;

		MyVector newA = oldSegment.a.subtract(center);
		MyVector newB = oldSegment.b.subtract(center);
		MyVector newC = oldSegment.a.subtract(oldSegment.b);
		double lenA = newA.getLen();
		double lenB = newB.getLen();
		double lenC = newC.getLen();
		double p = (lenA + lenB + lenC) / 2;
		double hc = (2 / lenC) * Math.sqrt(p * (p - lenA) * (p - lenB) * (p - lenC));
		if (hc > radius + 1.0e-07)
			return false;

		MySegment segment = new MySegment(newA, newB, oldSegment.tile);
		Line line = new Line(segment.a, segment.b);

		double a = line.a, b = line.b, c = line.c;

		double x0 = -a * c / (a * a + b * b), y0 = -b * c / (a * a + b * b);
		if (c * c > radius * radius * (a * a + b * b) + 1.0e-07)
			return false;
		else if (abs(c * c - radius * radius * (a * a + b * b)) < 1.0e-07) {
			double resang = atan2(y0, x0);
			return resang >= angle1 && resang <= angle2 && x0 >= min(segment.a.x, segment.b.x)
					&& x0 <= max(segment.a.x, segment.b.x) && y0 >= min(segment.a.y, segment.b.y)
					&& y0 <= max(segment.a.y, segment.b.y);
		}
		double d = radius * radius - c * c / (a * a + b * b);
		double mult = sqrt(d / (a * a + b * b));
		double ax, ay, bx, by;
		ax = x0 + b * mult;
		bx = x0 - b * mult;
		ay = y0 - a * mult;
		by = y0 + a * mult;
		double resang1 = atan2(ay, ax);
		if (resang1 >= angle1 && resang1 <= angle2 && ax >= min(segment.a.x, segment.b.x)
				&& ax <= max(segment.a.x, segment.b.x) && ay >= min(segment.a.y, segment.b.y)
				&& ay <= max(segment.a.y, segment.b.y))
			return true;
		double resang2 = atan2(by, bx);
		if (resang2 >= angle1 && resang2 <= angle2 && bx >= min(segment.a.x, segment.b.x)
				&& bx <= max(segment.a.x, segment.b.x) && by >= min(segment.a.y, segment.b.y)
				&& by <= max(segment.a.y, segment.b.y))
			return true;
		return false;
	}

	private MyVector getPoint1() {
		MyVector radiusVector = new MyVector(radius, 0);
		return radiusVector.rotate(angle1).add(center);
	}

	private MyVector getPoint2() {
		MyVector radiusVector = new MyVector(radius, 0);
		return radiusVector.rotate(angle2).add(center);
	}

	private MyVector maxVector = null;
	private MyVector minVector = null;

	private MyVector getMinVector() {
		if (minVector == null) {
			MyVector point1 = getPoint1();
			MyVector point2 = getPoint2();
			double minX = center.x;
			double minY = center.y;
			if (point1.x < minX)
				minX = point1.x;
			if (point1.y < minY)
				minY = point1.y;
			if (point2.x < minX)
				minX = point2.x;
			if (point2.y < minY)
				minY = point2.y;
			minVector = new MyVector(minX, minY);
		}
		return minVector;
	}

	private MyVector getMaxVector() {
		if (maxVector == null) {
			MyVector point1 = getPoint1();
			MyVector point2 = getPoint2();
			double maxX = center.x;
			double maxY = center.y;
			if (point1.x > maxX)
				maxX = point1.x;
			if (point1.y > maxY)
				maxY = point1.y;
			if (point2.x > maxX)
				maxX = point2.x;
			if (point2.y > maxY)
				maxY = point2.y;
			maxVector = new MyVector(maxX, maxY);
		}
		return maxVector;
	}

}

class Line implements MyGeometryObject {
	public final double a, b, c;

	public Line(MyVector a, MyVector b) {

		MyVector h = a.subtract(b);
		this.a = -h.y;
		this.b = h.x;
		this.c = -this.a * a.x - this.b * a.y;
	}

	@Override
	public boolean intersect(MySegment other) {
		return false; // TODO
	}

}
