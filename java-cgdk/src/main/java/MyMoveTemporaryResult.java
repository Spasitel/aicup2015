import java.util.List;
import java.util.Set;

import model.World;

public class MyMoveTemporaryResult {
	// initial parameters for the trajectory
	public final MyDecision decision;

	// result of the modeling
	public final MyGeometryObject collisionObject;
	public final MyTile.MovementAngleDifferenceInfo madInfo;
	public final int ticksTraveled;
	public final double enginePower;
	public final double wheelTurn;
	public final double angle;
	public final MyVector position;
	public final MyVector velocity;
	public final double distanceTraveled;
	
	public final boolean isEndReach;
	public final Set<List<MyTile>> paths;

	public final int stepInPath;

	public final Set<MyBonus> collectedBonuses;

	public MyMoveTemporaryResult(World world, MyDecision decision, MyGeometryObject collisionObject, int ticksTraveled,
			double enginePower, double wheelTurn, double angle, MyVector position, MyVector velocity,
			double distanceTraveled, Set<List<MyTile>> paths, boolean isEndReach, int stepInPath, Set<MyBonus> collectedBonuses) {
		this.decision = decision;

		this.collisionObject = collisionObject;
		this.ticksTraveled = ticksTraveled;
		this.enginePower = enginePower;
		this.wheelTurn = wheelTurn;
		this.angle = angle;
		this.position = position;
		this.velocity = velocity;
		this.distanceTraveled = distanceTraveled;
		
		this.isEndReach = isEndReach;
		this.paths = paths;
		this.stepInPath = stepInPath;
		this.collectedBonuses= collectedBonuses;

		this.madInfo = MyTile.getTile(position.x, position.y).getMovementAngleDifferenceInfo(world, angle);
	}

	public boolean isCollision() {
		return collisionObject != null;
	}

}