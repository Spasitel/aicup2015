public class MyDecision {
	public boolean isBrake() {
		return brake;
	}

	public double getWheelTurn() {
		return wheelTurn;
	}

	public double getEnginePower() {
		return enginePower;
	}

	public boolean isUseNitro() {
		return useNitro;
	}

	public void setUseNitro(boolean useNitro) {
		this.useNitro = useNitro;
	}

	public MyDecision(boolean brake, double wheelTurn, double enginePower) {
		super();
		this.brake = brake;
		this.wheelTurn = wheelTurn;
		this.enginePower = enginePower;
		useNitro = false;
	}

	private final boolean brake;
	private final double wheelTurn;
	private final double enginePower;
	private boolean useNitro;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (brake ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(enginePower);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(wheelTurn);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyDecision other = (MyDecision) obj;
		if (brake != other.brake)
			return false;
		if (Double.doubleToLongBits(enginePower) != Double.doubleToLongBits(other.enginePower))
			return false;
		if (Double.doubleToLongBits(wheelTurn) != Double.doubleToLongBits(other.wheelTurn))
			return false;
		return true;
	}

	public MyDecision copy() {
		return new MyDecision(this.brake, this.wheelTurn, this.enginePower);
	}

}