import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import model.Car;
import model.Game;
import model.World;

public class MyPathGenerator {
	private static boolean worldInitialized = false;
	private static boolean unknownMap = true;

	public static void generateAndSavePath(World world, Game game, Car car, MyTile prevTile) {
		if (world.getPlayers().length == 4) {
			if (!worldInitialized) {
				initializeWorld(world, game);
				worldInitialized = true;
				unknownMap = false;// TODO: save known maps
			}
		}
		MyTile startTile = MyTile.getTile(car.getX(), car.getY());
		Set<List<MyTile>> allPaths = generatePath(car, startTile, world, false, prevTile);
		Set<List<MyTile>> pathsWithoutUturn = getPathWithoutUturn(allPaths, prevTile, startTile);
		Set<List<MyTile>> result;
		if (pathsWithoutUturn.isEmpty()) {
			result = generatePath(car, startTile, world, true, prevTile);
			result.addAll(allPaths);
		} else {
			result = pathsWithoutUturn;
		}

		MyStateCalculator.PathFromTile path = new MyStateCalculator.PathFromTile(startTile, result, prevTile);
		MyStaticStorage.setPath(car.getType(), path);
	}

	private static Set<List<MyTile>> getPathWithoutUturn(Set<List<MyTile>> allPaths, MyTile prevTile, MyTile currTile) {
		Set<List<MyTile>> result = new LinkedHashSet<List<MyTile>>();
		path: for (List<MyTile> path : allPaths) {
			assert path.size() == MyMagicNumbers.NEXT_TILES_TO_CALCULATE;
			if (prevTile != null) {
				if (path.get(0).equals(prevTile))
					continue;
			}
			if (path.get(1).equals(currTile))
				continue;
			for (int i = 2; i < path.size(); i++) {
				if (path.get(i).equals(path.get(i - 2)))
					continue path;
			}
			result.add(path);
		}
		return result;
	}

	private static Set<List<MyTile>> generatePath(Car car, MyTile startTile, World world, boolean isCheckUturn,
			MyTile prevTile) {
		Set<List<MyTile>> paths = new LinkedHashSet<List<MyTile>>();

		int distanceToTravel = MyMagicNumbers.NEXT_TILES_TO_CALCULATE;
		int nextWaypointIndex = car.getNextWaypointIndex();
		MyTile nextStartTile = startTile;

		while (distanceToTravel > 0) {
			MyTile nextWaypointTile = MyTile.tiles[world.getWaypoints()[nextWaypointIndex][0]][world.getWaypoints()[nextWaypointIndex][1]];

			int[][] bfsForWaypoint = getSavedBFSForWaypoint(world, nextWaypointTile);
			int distanceToWaypoint = bfsForWaypoint[nextStartTile.x][nextStartTile.y];
			if (isCheckUturn) {
				MyTile prevTileInPath;
				if (paths.isEmpty())
					prevTileInPath = prevTile;
				else {
					List<MyTile> next = paths.iterator().next();
					if (next.size() > 1)
						prevTileInPath = next.get(next.size() - 2);
					else
						prevTileInPath = startTile;
				}

				int[][] bfsForWaypointWithoutUTurn = getBFSForWaypointWithoutUTurn(world, nextWaypointTile,
						prevTileInPath, nextStartTile);
				int distanceToWaypointWithoutUTurn = bfsForWaypointWithoutUTurn[nextStartTile.x][nextStartTile.y];
				
				
				if(!isDoubleUTurn(nextStartTile, bfsForWaypointWithoutUTurn, nextWaypointIndex, world, nextWaypointTile))
					if (distanceToWaypointWithoutUTurn - distanceToWaypoint < 12) {
						bfsForWaypoint = bfsForWaypointWithoutUTurn;
						distanceToWaypoint = distanceToWaypointWithoutUTurn;
					}
			}

			int traveledDistance = distanceToTravel > distanceToWaypoint ? distanceToWaypoint : distanceToTravel;
			Set<List<MyTile>> pathsFromDFS = reversDFS(nextStartTile, bfsForWaypoint, traveledDistance, world);
			if (paths.isEmpty()) {
				paths.addAll(pathsFromDFS);
			} else {
				Set<List<MyTile>> tmp = new LinkedHashSet<List<MyTile>>();
				for (List<MyTile> firtsPart : paths) {
					for (List<MyTile> secondPart : pathsFromDFS) {
						List<MyTile> result = new LinkedList<MyTile>();
						result.addAll(firtsPart);
						result.addAll(secondPart);
						// TODO: U-turn
						tmp.add(result);
					}
				}
				paths = tmp;
			}

			nextStartTile = nextWaypointTile;
			distanceToTravel -= traveledDistance;
			nextWaypointIndex = (nextWaypointIndex + 1) % world.getWaypoints().length;
		}
		return paths;
	}

	private static boolean isDoubleUTurn(MyTile nextStartTile, int[][] bfsForWaypoint, int waypointIndex, World world, MyTile waypointTile) {

		Set<MyTile> current = new LinkedHashSet<MyTile>();
		Set<MyTile> next = new LinkedHashSet<MyTile>();
		int level = bfsForWaypoint[nextStartTile.x][nextStartTile.y]-1;
		current.add(nextStartTile);

		while (level > 0) {
			for (MyTile currentTile : current) {
				for (MyTile connected : currentTile.getConnectedTiles(world)) {
					if (bfsForWaypoint[connected.x][connected.y] == level)
						next.add(connected);
				}
			}

			current = next;
			next = new LinkedHashSet<MyTile>();
			level--;
		}
		
		if(current.size() > 1)
			return false;
		
		MyTile nextTileBeforeWaypoint = current.iterator().next();
		int nextWaypointIndex = (waypointIndex + 1) % world.getWaypoints().length;
		MyTile nextWaypointTile = MyTile.tiles[world.getWaypoints()[nextWaypointIndex][0]][world.getWaypoints()[nextWaypointIndex][1]];

		int[][] bfsForNextWaypoint = getSavedBFSForWaypoint(world, nextWaypointTile);
		int levelNext = bfsForNextWaypoint[waypointTile.x][waypointTile.y];
		
		for(MyTile connected: waypointTile.getConnectedTiles(world)){
			if(bfsForNextWaypoint[connected.x][connected.y] + 1 == levelNext && !connected.equals(nextTileBeforeWaypoint))
				return false;
		}
		
		return true;
	}

	public static int[][] getSavedBFSForWaypoint(World world, MyTile nextWaypointTile) {
		if (!unknownMap)
			return MyStaticStorage.getBFSForWaypoint(nextWaypointTile);
		else {
			return getBFSForWaypoint(world, nextWaypointTile);
		}
	}

	private static Set<List<MyTile>> reversDFS(MyTile startTile, int[][] bfsForWaypoint, int nextTilesToCalculate,
			World world) {
		Set<List<MyTile>> result = new LinkedHashSet<List<MyTile>>();

		List<List<MyTile>> toWatch = new LinkedList<List<MyTile>>();

		int startIndex = bfsForWaypoint[startTile.x][startTile.y];
		for (MyTile neighbor : startTile.getConnectedTiles(world)) {
			if (bfsForWaypoint[neighbor.x][neighbor.y] == startIndex - 1) {
				LinkedList<MyTile> start = new LinkedList<MyTile>();
				start.add(neighbor);
				toWatch.add(start);
			}
		}

		while (!toWatch.isEmpty()) {
			List<MyTile> current = toWatch.remove(0);

			int size = current.size();
			if (size == nextTilesToCalculate) {
				result.add(current);
			} else {
				assert size < nextTilesToCalculate;
				MyTile last = current.get(size - 1);
				int index = bfsForWaypoint[last.x][last.y];
				if (index <= 0)
					assert index > 0;
				for (MyTile neighbor : last.getConnectedTiles(world)) {
					if (bfsForWaypoint[neighbor.x][neighbor.y] == index - 1) {
						List<MyTile> newPath = new LinkedList<MyTile>();
						newPath.addAll(current);
						newPath.add(neighbor);
						toWatch.add(0, newPath);
					}
				}
			}

		}

		return result;
	}

	private static void initializeWorld(World world, Game game) {
		for (int[] waypoint : world.getWaypoints()) {
			MyTile waypointTile = MyTile.tiles[waypoint[0]][waypoint[1]];
			if (MyStaticStorage.getBFSForWaypoint(waypointTile) == null) {
				int[][] resultBFS = getBFSForWaypoint(world, waypointTile);

				MyStaticStorage.setBFSForWatpoint(waypointTile, resultBFS);
			}
		}

	}

	public static int[][] getBFSForWaypoint(World world, MyTile waypointTile) {
		return getBFSForWaypointWithoutUTurn(world, waypointTile, null, null);
	}

	public static int[][] getBFSForWaypointWithoutUTurn(World world, MyTile waypointTile, MyTile prevUturn,
			MyTile currentUTurn) {
		int[][] resultBFS = new int[world.getWidth()][world.getHeight()];

		for (int i = 0; i < resultBFS.length; i++)
			for (int k = 0; k < resultBFS[0].length; k++)
				resultBFS[i][k] = -1;

		Set<MyTile> current = new LinkedHashSet<MyTile>();
		Set<MyTile> next = new LinkedHashSet<MyTile>();
		int level = 0;
		current.add(waypointTile);

		while (!current.isEmpty()) {
			for (MyTile currentTile : current) {
				assert resultBFS[currentTile.x][currentTile.y] == -1;
				resultBFS[currentTile.x][currentTile.y] = level;
				for (MyTile connected : currentTile.getConnectedTiles(world)) {
					if (prevUturn != null && currentUTurn != null) {
						if (currentTile.equals(prevUturn) && connected.equals(currentUTurn))
							continue;
					}

					if (resultBFS[connected.x][connected.y] == -1)
						next.add(connected);
				}
			}

			current = next;
			next = new LinkedHashSet<MyTile>();
			level++;
		}
		return resultBFS;
	}

	static boolean is180TurnAfterPath(List<MyTile> path, Car self, World world, Game game) {
		if(path.size()<3)
			return false;//TODO:
		
		MyTile first = path.get(path.size()-3);
		MyTile second = path.get(path.size()-2);
		MyTile thurd = path.get(path.size()-1);
		
		int nextWayppiontIndex = self.getNextWaypointIndex();
		for(MyTile tile: path){
			if(world.getWaypoints()[nextWayppiontIndex][0] == tile.x &&
					world.getWaypoints()[nextWayppiontIndex][1] == tile.y)
				nextWayppiontIndex = (nextWayppiontIndex+1)%world.getWaypoints().length;
		}
		
		MyTile waypointTile = MyTile.tiles[world.getWaypoints()[nextWayppiontIndex][0]][world.getWaypoints()[nextWayppiontIndex][1]];
		int[][] bfs = getBFSForWaypoint(world, waypointTile);
		int thrurdLvl = bfs[thurd.x][thurd.y];
		MyTile fourht = null;
		for(MyTile connTile: thurd.getConnectedTiles(world)){
			if(bfs[connTile.x][connTile.y] == thrurdLvl-1){
				if(fourht != null)
					return false;
				fourht = connTile;
			}
		}
		
		if(first.x != thurd.x && first.y != thurd.y && second.x != fourht.x && second.y != fourht.y &&
				second.x - first.x == thurd.x - fourht.x &&
				second.y - first.y == thurd.y - fourht.y)
			return true;
		
		return false;
	}

}
