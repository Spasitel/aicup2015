import model.Car;
import model.Game;
import model.World;

public class MyOilSpiller {

	public static boolean isSpillOil(Car self, World world, Game game) {
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());

		boolean isSpill = false;
		loop: for (Car car : world.getCars()) {
			if (car.isTeammate() && car.getType().equals(self.getType()))
				continue;
			MyTile carTile = MyTile.getTile(car.getX(), car.getY());
			for (MyStaticStorage.HistoryStep step : MyStaticStorage.getHistoryPath(self.getType())) {
				if (step.nextWaypointIndex == car.getNextWaypointIndex() && step.tile.equals(carTile) && !carTile.equals(selfTile)) {
					if (car.isTeammate()) {
						isSpill = false;
						break loop;
					}else{
						if(world.getTick() - step.tick < game.getOilSlickLifetime() *2/3)
							isSpill = true;
					}
				}
			}
		}

		MyVector oilDistance = new MyVector(game.getOilSlickInitialRange()+ game.getOilSlickRadius() + game.getCarWidth()/2,0);
		MyVector selfCenter = new MyVector(self.getX(), self.getY());
		MyVector oilCenter = oilDistance.rotate(self.getAngle()).multiply(-1).add(selfCenter);

		MyStaticStorage.SpillOilData spillOilData = MyStaticStorage.getSpillOilTile(self.getType());
		if (spillOilData != null) {
			if (spillOilData.fisrtTile.equals(selfTile) || spillOilData.secondTile.equals(selfTile)) {
				boolean goodPos = false;
				double oil;
				double tile;
				double tileX = game.getTrackTileSize() * selfTile.x + game.getTrackTileSize()/2;
				double tileY = game.getTrackTileSize() * selfTile.y + game.getTrackTileSize()/2;
				if(spillOilData.isX){
					oil = oilCenter.x;
					tile = tileX;
					goodPos = (spillOilData.fisrtTile.y - spillOilData.secondTile.y)*(tileY - oilCenter.y) >0;
				}else{
					oil = oilCenter.y;
					tile = tileY;
					goodPos = (spillOilData.fisrtTile.x - spillOilData.secondTile.x)*(tileX - oilCenter.x) >0;
				}

				double best = spillOilData.bestPlace;
				tile = (tile+best)/2;
				if(goodPos || spillOilData.secondTile.equals(selfTile))
					if((oil - best)*(tile-best) > 0 && (tile - oil) * (tile - best) >0 ){
						MyStaticStorage.setSpillOilTile(self.getType(), null);
						if(isSpill)
							return true;
					}
			}
		}

		
		boolean result = false;
		for (Car car : world.getCars()) {
			if (car.isTeammate() && car.getType().equals(self.getType()))
				continue;
			
			MyVector carCenter = new MyVector(car.getX(), car.getY());
			MyVector fromOilToCar = oilCenter.subtract(carCenter);
			if(fromOilToCar.getLen() < game.getOilSlickRadius()){
				if(car.isTeammate())
					return false;
				double carSpeed = new MyVector(car.getSpeedX(), car.getSpeedY()).getLen();
				if(carSpeed > 20 || (carSpeed > 15 && car.getRemainingNitroTicks() > 10))
					return  true;
			}
		}

		return result;
	}
}
