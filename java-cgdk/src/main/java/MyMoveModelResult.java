import java.util.List;
import java.util.Set;

import model.Car;


public class MyMoveModelResult {
	
	private final Car car;
	private final boolean isEndReached;
	private final int stepsModeled;
	private final Set<List<MyTile>> paths;
	private final int stepInPath;
	private final Set<MyBonus> collectedBonuses;
	
	public MyMoveModelResult(Car car, boolean isEndReached, int stepsModeled, Set<List<MyTile>> paths,int stepInPath, Set<MyBonus> collectedBonuses) {
		super();
		this.car = car;
		this.isEndReached = isEndReached;
		this.stepsModeled = stepsModeled;
		this.paths = paths;
		this.stepInPath = stepInPath;
		this.collectedBonuses = collectedBonuses;
	}

	public Car getCar() {
		return car;
	}

	public boolean isEndReached() {
		return isEndReached;
	}

	public int getStepsModeled() {
		return stepsModeled;
	}

	public Set<List<MyTile>> getPaths() {
		return paths;
	}

	public int getStepInPath() {
		return stepInPath;
	}

	public Set<MyBonus> getCollectedBonuses() {
		return collectedBonuses;
	}

}
