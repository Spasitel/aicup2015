import java.util.List;
import java.util.Set;

import model.Car;
import model.Game;
import model.World;

public class MyStateCalculator {

	/*
	 * Calculate optimal path in the start, calculate next 2 tails
	 */
	public static Set<List<MyTile>> calculatePath(Car self, World world, Game game) {


		MyTile currentTile = MyTile.getTile(self.getX(), self.getY());

		PathFromTile path = MyStaticStorage.getPath(self.getType());
		if (path == null || !path.getStartTile().equals(currentTile)) {
			MyTile prevTile;
			if(path == null)
				prevTile = null;
			else
				prevTile = path.getStartTile();
			MyPathGenerator.generateAndSavePath(world, game, self, prevTile);
		}

		return MyStaticStorage.getPath(self.getType()).getPaths();

	}
	
	public static class PathFromTile{
		public PathFromTile(MyTile startTile, Set<List<MyTile>> paths, MyTile prevTile) {
			super();
			this.prevTile = prevTile;
			this.startTile = startTile;
			this.paths = paths;
		}
		private final MyTile prevTile;
		private final MyTile startTile;
		private final Set<List<MyTile>> paths;
		public MyTile getStartTile() {
			return startTile;
		}
		public Set<List<MyTile>> getPaths() {
			return paths;
		}
		public MyTile getPrevTile() {
			return prevTile;
		}
		
	}

}
