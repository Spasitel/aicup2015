import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import model.Bonus;
import model.Car;
import model.Game;
import model.Player;
import model.World;

public class MyEvaluationBonuses {

	public MyEvaluationBonuses(Set<MyBonus> allBonusesInPaths, int bonusForAmmo, int bonusForNitro, int bonusForOil,
			int bonusForScore, int bonusForRepare, int startEvaluation) {
		super();
		this.allBonusesInPaths = allBonusesInPaths;
		this.bonusForAmmo = bonusForAmmo;
		this.bonusForNitro = bonusForNitro;
		this.bonusForOil = bonusForOil;
		this.bonusForScore = bonusForScore;
		this.bonusForRepare = bonusForRepare;
		this.startEvaluation = startEvaluation;
	}

	private final Set<MyBonus> allBonusesInPaths;

	private final int bonusForAmmo;
	private final int bonusForNitro;
	private final int bonusForOil;
	private final int bonusForScore;
	private final int bonusForRepare;

	private final int startEvaluation;

	public static MyEvaluationBonuses create(Car self, World world, Game game, Set<List<MyTile>> pathes) {
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());
		Set<MyTile> tilesInPaths = new LinkedHashSet<MyTile>();
		tilesInPaths.add(selfTile);
		for (List<MyTile> path : pathes) {
			tilesInPaths.addAll(path);
		}

		Set<MyBonus> allBonusesInPaths = new LinkedHashSet<MyBonus>();

		int bonusForAmmo = 0;
		int bonusForNitro = 0;
		int bonusForOil = 0;
		int bonusForScore = 0;

		boolean isRepareNeed = true;
		int bonusForRepare = 0;

		int startEvaluation = 0;

		for (Bonus bonus : world.getBonuses()) {
			MyTile bonusTile = MyTile.getTile(bonus.getX(), bonus.getY());
			if (!tilesInPaths.contains(bonusTile))
				continue;

			MyBonus myBonus = new MyBonus(bonus.getType(), bonus.getX(), bonus.getY(), bonusTile);
			allBonusesInPaths.add(myBonus);

			switch (myBonus.getType()) {
			case AMMO_CRATE:
				if (bonusForAmmo == 0)
					bonusForAmmo = getBonusForAmmo(self, world, game);
				startEvaluation += bonusForAmmo;
				break;
			case NITRO_BOOST:
				if (bonusForNitro == 0)
					bonusForNitro = getBonusForNitro(self, world, game);
				startEvaluation += bonusForNitro;
				break;
			case OIL_CANISTER:
				if (bonusForOil == 0)
					bonusForOil = getBonusForOil(self, world, game);
				startEvaluation += bonusForOil;
				break;
			case PURE_SCORE:
				if (bonusForScore == 0)
					bonusForScore = getBonusForScore(self, world, game);
				startEvaluation += bonusForScore;
				break;
			case REPAIR_KIT:
				if (isRepareNeed) {
					if (bonusForRepare == 0)
						bonusForRepare = getBonusForRepare(self, world, game);
					startEvaluation += bonusForRepare;
					isRepareNeed = false;
				}
				break;
			default:
				assert false;
				break;
			}
		}

		return new MyEvaluationBonuses(allBonusesInPaths, bonusForAmmo, bonusForNitro, bonusForOil, bonusForScore,
				bonusForRepare, startEvaluation);
	}

	private static int getBonusForAmmo(Car self, World world, Game game) {
		return (int) (30.0 * getRatio(self.getNextWaypointIndex(), self.getProjectileCount(), world, self.getPlayerId()));
	}

	private static int getBonusForNitro(Car self, World world, Game game) {
		
		return (int) (15.0 * getRatio(self.getNextWaypointIndex(), self.getNitroChargeCount(), world, self.getPlayerId()));
	}

	private static int getBonusForOil(Car self, World world, Game game) {
		return (int) (10.0 * getRatio(self.getNextWaypointIndex(), self.getOilCanisterCount(), world, self.getPlayerId()));
	}

	private static int getBonusForScore(Car self, World world, Game game) {
		return (int)(world.getTickCount() - 2000.0 / 160.0);
	}

	private static int getBonusForRepare(Car self, World world, Game game) {
		return 100 - (int) (self.getDurability());
	}

	private static double getRatio(int nextWaypointIndex, int projectileCount, World world, long playerId) {
		int countForSecondLap;
		if(world.getPlayers().length == 2)
			countForSecondLap = 2000;
		else
			countForSecondLap = 1000;
		boolean isSecondLap = false;
		for(Player player:world.getPlayers()){
			if(player.getId() == playerId){
				isSecondLap = player.getScore() > countForSecondLap;
				break;
			}
		}
		int length = world.getWaypoints().length;
		if(nextWaypointIndex == 0)
			nextWaypointIndex = length;
		if(!isSecondLap || nextWaypointIndex <= length / 2)
			return 1;
		
		return (double)((length - nextWaypointIndex) * 2.0) / (double)length;
	}

	public int getStateEvaluation(Car endState, World world, Game game, Set<MyBonus> collected) {
		int result = startEvaluation;
		boolean isRepareCollected = false;
		for(MyBonus bonus:collected){
			switch (bonus.getType()) {
			case AMMO_CRATE:
				result -= bonusForAmmo;
				break;
			case NITRO_BOOST:
				result -= bonusForNitro;
				break;
			case OIL_CANISTER:
				result -= bonusForOil;
				break;
			case PURE_SCORE:
				result -= bonusForScore;
				break;
			case REPAIR_KIT:
				if(!isRepareCollected){
					result -= bonusForRepare;
					isRepareCollected = true;
				}
				break;
			default:
				assert false;
				break;
			}
		}
		
		if(result < 0){
			assert false;
			return 0;
		}
		
		return result;
	}

	public Set<MyBonus> adaptateBonuses(Set<MyBonus> collectedBonuses) {
		Set<MyBonus> result = new LinkedHashSet<MyBonus>();
		result.addAll(collectedBonuses);
		if(result.retainAll(allBonusesInPaths))
			MyStrategy.debug(-2, "BONUS SNATCHED");
		return result;
	}

	public Set<MyBonus> getAllBonuses() {
		return allBonusesInPaths;
	}

}
