import static java.lang.Math.*;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import model.Car;
import model.CarType;
import model.Game;
import model.TileType;
import model.World;

public class MyTile {
	private static boolean worldInitialized = false;
	public static MyTile[][] tiles = null;
	public static double tileSize = 0.0;
	public static double tileMargin = 0.0;

	private static EnumMap<TileType, double[]> movementAngles;
	private static TileType[][] tileTypes = null;

	public final int x, y;
	private List<MyGeometryObject> tileGeometry;

	@Override
	public int hashCode() {
		return x * 100 + y;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof MyTile))
			return false;
		MyTile otherTile = (MyTile) o;
		return this.x == otherTile.x && this.y == otherTile.y;
	}

	private MyTile(int x, int y, Game game, World world) {
		if (worldInitialized)
			throw new IllegalStateException();
		this.x = x;
		this.y = y;
		List<MyGeometryObject> geometryList = createTileGeometry(game, world, this);
		if (geometryList != null) {
			this.tileGeometry = geometryList;
		} else {
			this.tileGeometry = null;
		}
	}

	public static void initWorld(Car self, Game game, World world) {
		tileTypes = world.getTilesXY();

		if (!worldInitialized) {
			tileSize = game.getTrackTileSize();
			tileMargin = game.getTrackTileMargin();

			tiles = new MyTile[world.getWidth()][world.getHeight()];
			for (int x = 0; x < world.getWidth(); ++x)
				for (int y = 0; y < world.getHeight(); ++y) {
					tiles[x][y] = new MyTile(x, y, game, world);
				}

			movementAngles = new EnumMap<TileType, double[]>(TileType.class);
			movementAngles.put(TileType.BOTTOM_HEADED_T, new double[] { 0.0, PI / 2, PI });
			movementAngles.put(TileType.CROSSROADS, new double[] { 0.0, PI / 2, PI, -PI / 2 });
			movementAngles.put(TileType.EMPTY, new double[] {});
			movementAngles.put(TileType.HORIZONTAL, new double[] { 0.0, PI });
			movementAngles.put(TileType.LEFT_BOTTOM_CORNER, new double[] { 0.0, -PI / 2 });
			movementAngles.put(TileType.LEFT_HEADED_T, new double[] { PI / 2, PI, -PI / 2 });
			movementAngles.put(TileType.LEFT_TOP_CORNER, new double[] { 0.0, PI / 2 });
			movementAngles.put(TileType.RIGHT_BOTTOM_CORNER, new double[] { PI, -PI / 2 });
			movementAngles.put(TileType.RIGHT_HEADED_T, new double[] { 0.0, PI / 2, -PI / 2 });
			movementAngles.put(TileType.RIGHT_TOP_CORNER, new double[] { PI / 2, PI });
			movementAngles.put(TileType.TOP_HEADED_T, new double[] { 0.0, PI, -PI / 2 });
			movementAngles.put(TileType.UNKNOWN, new double[] {});
			movementAngles.put(TileType.VERTICAL, new double[] { PI / 2, -PI / 2 });

			worldInitialized = true;
		}

		updateHistoryPath(self, game, world);
	}

	private static void updateHistoryPath(Car self, Game game, World world) {
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());
		CarType type = self.getType();
		if (!selfTile.equals(MyStaticStorage.getCurrentTile(type))) {
			int tick = world.getTick();
			MyStaticStorage.HistoryStep newStep = new MyStaticStorage.HistoryStep(tick, selfTile,
					self.getNextWaypointIndex());
			List<MyStaticStorage.HistoryStep> path = MyStaticStorage.getHistoryPath(type);

			while (!path.isEmpty()) {
				MyStaticStorage.HistoryStep step = path.get(0);
				if (tick - step.tick > game.getOilSlickLifetime())
					path.remove(0);
				else
					break;
			}
			path.add(newStep);
			MyStaticStorage.setCurrentTile(type, selfTile);
		}

	}

	public TileType getType(World world) {
		TileType tileType = tileTypes[x][y];
		assert world.getTilesXY()[x][y].equals(tileType) : "world: " + world.getTilesXY()[x][y] + " saved: " + tileType
				+ " " + x + " " + y;
		return tileType;
	}

	public static class MovementAngleDifferenceInfo {
		public final double movementAngleDifference;
		public final double closestMovementAngle;
		public final boolean turnLeft; // to closestAngle

		public MovementAngleDifferenceInfo(double a, double b, boolean c) {
			this.movementAngleDifference = a;
			this.closestMovementAngle = b;
			this.turnLeft = c;
		}
	}

	public MovementAngleDifferenceInfo getMovementAngleDifferenceInfo(World world, double angle) {
		double closestMovementAngleDiff = 2 * PI;
		double closestMovementAngle = 0.0;
		TileType type = world.getTilesXY()[this.x][this.y];
		for (double movementAngle : movementAngles.get(type)) {
			double angleDifference = abs(MyMoveTrajectoryModeler.normalizeAngle(angle - movementAngle));
			if (angleDifference < closestMovementAngleDiff) {
				closestMovementAngleDiff = angleDifference;
				closestMovementAngle = movementAngle;
			}
		}
		boolean isLeftRotation = MyMoveTrajectoryModeler.isLeftRotation(angle, closestMovementAngle);
		return new MovementAngleDifferenceInfo(closestMovementAngleDiff, closestMovementAngle, isLeftRotation);
	}

	public static MyTile getTile(double x, double y) {
		int stx = (int) (x / tileSize);
		int sty = (int) (y / tileSize);
		if (stx < 0 || stx >= MyTile.tiles.length || sty < 0 || sty >= MyTile.tiles[0].length)
			return null;
		return MyTile.tiles[stx][sty];
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ") ";
	}

	public static MyGeometryObject getTileCollision(MyVector playerCenter, double angle, double playerHeight,
			double playerWidth, Game game, World world) {
		MyVector[] corners = MyPlayerGeometry.playerCorners(playerCenter, angle, playerHeight, playerWidth);
		MyTile[] cornerTile = new MyTile[4];
		for (int i = 0; i < 4; ++i) {
			if (corners[i].x > tiles.length * tileSize - tileMargin)
				corners[i].toString();
			if (corners[i].y > tiles[0].length * tileSize - tileMargin)
				corners[i].toString();
			cornerTile[i] = getTile(corners[i].x, corners[i].y);
		}

		for (int i = 0; i < 4; ++i) {
			double cornerX = corners[i].x - cornerTile[i].x * tileSize;
			double cornerY = corners[i].y - cornerTile[i].y * tileSize;
			assert cornerX >= 0 && cornerX <= 800;
			assert cornerY >= 0 && cornerY <= 800;
			if (tileMargin >= cornerX - playerWidth || cornerX + playerWidth >= tileSize - tileMargin
					|| tileMargin >= cornerY - playerWidth || cornerY + playerWidth >= tileSize - tileMargin) {

				MySegment playerSegment = new MySegment(corners[i], corners[(i + 1) % 4], null);

				for (MyGeometryObject o : cornerTile[i].getTileGeometry(game, world))
					if (o.intersect(playerSegment))
						return o;
				if (!cornerTile[i].equals(cornerTile[(i + 1) % 4]))
					for (MyGeometryObject o : cornerTile[(i + 1) % 4].getTileGeometry(game, world))
						if (o.intersect(playerSegment))
							return o;

			}

		}

		for (int i = 0; i < 4; ++i) {
			if (corners[i].x > tiles.length * tileSize - tileMargin)
				assert false;
			if (corners[i].y > tiles[0].length * tileSize - tileMargin)
				assert false;
		}

		return null;
	}

	private final static List<MyGeometryObject> empty = new ArrayList<MyGeometryObject>();

	private List<MyGeometryObject> getTileGeometry(Game game, World world) {
		if (getType(world).equals(TileType.UNKNOWN))
			return empty;
		if (tileGeometry == null)
			tileGeometry = createTileGeometry(game, world, this);

		return tileGeometry;
	}

	private static List<MyGeometryObject> createTileGeometry(Game game, World world, MyTile tile) {

		List<MyGeometryObject> res = new ArrayList<MyGeometryObject>();

		double tile_x = tile.x * tileSize;
		double tile_y = tile.y * tileSize;

		switch (tile.getType(world)) {
		case BOTTOM_HEADED_T:
			res.add(new MySegment(tile_x, tile_y + tileMargin, tile_x + tileSize, tile_y + tileMargin, tile));
			res.add(new MyArc(tile_x + tileSize, tile_y + tileSize, tileMargin, -PI, -PI / 2, tile));
			res.add(new MyArc(tile_x, tile_y + tileSize, tileMargin, -PI / 2, 0, tile));
			break;
		case CROSSROADS:
			res.add(new MyArc(tile_x, tile_y, tileMargin, 0, PI / 2, tile));
			res.add(new MyArc(tile_x + tileSize, tile_y, tileMargin, PI / 2, PI, tile));
			res.add(new MyArc(tile_x + tileSize, tile_y + tileSize, tileMargin, -PI, -PI / 2, tile));
			res.add(new MyArc(tile_x, tile_y + tileSize, tileMargin, -PI / 2, 0, tile));
			break;
		case EMPTY:
			break;
		case HORIZONTAL:
			res.add(new MySegment(tile_x, tile_y + tileMargin, tile_x + tileSize, tile_y + tileMargin, tile));
			res.add(new MySegment(tile_x, tile_y + tileSize - tileMargin, tile_x + tileSize, tile_y + tileSize
					- tileMargin, tile));
			break;
		case LEFT_BOTTOM_CORNER:
			res.add(new MyArc(tile_x + tileSize, tile_y, tileMargin, PI / 2, PI, tile));
			res.add(new MyArc(tile_x + 2 * tileMargin, tile_y + tileSize - 2 * tileMargin, tileMargin, PI / 2, PI, tile));
			res.add(new MySegment(tile_x + tileMargin, tile_y, tile_x + tileMargin, tile_y + tileSize - 2 * tileMargin,
					tile));
			res.add(new MySegment(tile_x + 2 * tileMargin, tile_y + tileSize - tileMargin, tile_x + tileSize, tile_y
					+ tileSize - tileMargin, tile));
			break;
		case LEFT_HEADED_T:
			res.add(new MyArc(tile_x, tile_y, tileMargin, 0, PI / 2, tile));
			res.add(new MyArc(tile_x, tile_y + tileSize, tileMargin, -PI / 2, 0, tile));
			res.add(new MySegment(tile_x + tileSize - tileMargin, tile_y, tile_x + tileSize - tileMargin, tile_y
					+ tileSize, tile));
			break;
		case LEFT_TOP_CORNER:
			res.add(new MyArc(tile_x + tileSize, tile_y + tileSize, tileMargin, -PI, -PI / 2, tile));
			res.add(new MyArc(tile_x + 2 * tileMargin, tile_y + 2 * tileMargin, tileMargin, -PI, -PI / 2, tile));
			res.add(new MySegment(tile_x + tileMargin, tile_y + 2 * tileMargin, tile_x + tileMargin, tile_y + tileSize,
					tile));
			res.add(new MySegment(tile_x + 2 * tileMargin, tile_y + tileMargin, tile_x + tileSize, tile_y + tileMargin,
					tile));
			break;
		case RIGHT_BOTTOM_CORNER:
			res.add(new MyArc(tile_x, tile_y, tileMargin, 0, PI / 2, tile));
			res.add(new MyArc(tile_x + tileSize - 2 * tileMargin, tile_y + tileSize - 2 * tileMargin, tileMargin, 0,
					PI / 2, tile));
			res.add(new MySegment(tile_x, tile_y + tileSize - tileMargin, tile_x + tileSize - 2 * tileMargin, tile_y
					+ tileSize - tileMargin, tile));
			res.add(new MySegment(tile_x + tileSize - tileMargin, tile_y, tile_x + tileSize - tileMargin, tile_y
					+ tileSize - 2 * tileMargin, tile));
			break;
		case RIGHT_HEADED_T:
			res.add(new MySegment(tile_x + tileMargin, tile_y, tile_x + tileMargin, tile_y + tileSize, tile));
			res.add(new MyArc(tile_x + tileSize, tile_y, tileMargin, PI / 2, PI, tile));
			res.add(new MyArc(tile_x + tileSize, tile_y + tileSize, tileMargin, -PI, -PI / 2, tile));
			break;
		case RIGHT_TOP_CORNER:
			res.add(new MyArc(tile_x, tile_y + tileSize, tileMargin, -PI / 2, 0, tile));
			res.add(new MyArc(tile_x + tileSize - 2 * tileMargin, tile_y + 2 * tileMargin, tileMargin, -PI / 2, 0, tile));
			res.add(new MySegment(tile_x, tile_y + tileMargin, tile_x + tileSize - 2 * tileMargin, tile_y + tileMargin,
					tile));
			res.add(new MySegment(tile_x + tileSize - tileMargin, tile_y + 2 * tileMargin, tile_x + tileSize
					- tileMargin, tile_y + tileSize, tile));
			break;
		case TOP_HEADED_T:
			res.add(new MyArc(tile_x, tile_y, tileMargin, 0, PI / 2, tile));
			res.add(new MyArc(tile_x + tileSize, tile_y, tileMargin, PI / 2, PI, tile));
			res.add(new MySegment(tile_x, tile_y + tileSize - tileMargin, tile_x + tileSize, tile_y + tileSize
					- tileMargin, tile));
			break;
		case UNKNOWN:
			return null;
		case VERTICAL:
			res.add(new MySegment(tile_x + tileMargin, tile_y, tile_x + tileMargin, tile_y + tileSize, tile));
			res.add(new MySegment(tile_x + tileSize - tileMargin, tile_y, tile_x + tileSize - tileMargin, tile_y
					+ tileSize, tile));
			break;
		default:
			break;

		}

		return res;
	}

	public MyTile getBot() {
		try {
			return MyTile.tiles[x][y + 1];
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public MyTile getTop() {
		try {
			return MyTile.tiles[x][y - 1];
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public MyTile getLeft() {
		try {
			return MyTile.tiles[x - 1][y];
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public MyTile getRight() {
		try {
			return MyTile.tiles[x + 1][y];
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public Set<MyTile> getConnectedTiles(World world) {
		TileType type = getType(world);
		Set<MyTile> result = new LinkedHashSet<MyTile>();
		switch (type) {
		case BOTTOM_HEADED_T:
			addBot(result, world);
			addRight(result, world);
			addLeft(result, world);
			break;
		case CROSSROADS:
			addBot(result, world);
			addTop(result, world);
			addRight(result, world);
			addLeft(result, world);
			break;
		case EMPTY:
			break;
		case HORIZONTAL:
			addRight(result, world);
			addLeft(result, world);
			break;
		case LEFT_BOTTOM_CORNER:
			addTop(result, world);
			addRight(result, world);
			break;
		case LEFT_HEADED_T:
			addBot(result, world);
			addTop(result, world);
			addLeft(result, world);
			break;
		case LEFT_TOP_CORNER:
			addBot(result, world);
			addRight(result, world);
			break;
		case RIGHT_BOTTOM_CORNER:
			addTop(result, world);
			addLeft(result, world);
			break;
		case RIGHT_HEADED_T:
			addBot(result, world);
			addTop(result, world);
			addRight(result, world);
			break;
		case RIGHT_TOP_CORNER:
			addBot(result, world);
			addLeft(result, world);
			break;
		case TOP_HEADED_T:
			addTop(result, world);
			addRight(result, world);
			addLeft(result, world);
			break;
		case UNKNOWN:
			Set<MyTile> tmp = new HashSet<MyTile>();
			addBot(tmp, world);
			addTop(tmp, world);
			addRight(tmp, world);
			addLeft(tmp, world);
			for (MyTile connect : tmp) {
				if (connect.getType(world).equals(TileType.UNKNOWN))
					result.add(connect);
				else if (connect.getConnectedTiles(world).contains(this))
					result.add(connect);
			}
			break;
		case VERTICAL:
			addBot(result, world);
			addTop(result, world);
			break;
		default:
			break;
		}

		return result;
	}

	private void addBot(Set<MyTile> result, World world) {
		MyTile nextTile = getBot();
		if (nextTile != null)
			result.add(nextTile);
	}

	private void addTop(Set<MyTile> result, World world) {
		MyTile nextTile = getTop();
		if (nextTile != null)
			result.add(nextTile);
	}

	private void addLeft(Set<MyTile> result, World world) {
		MyTile nextTile = getLeft();
		if (nextTile != null)
			result.add(nextTile);
	}

	private void addRight(Set<MyTile> result, World world) {
		MyTile nextTile = getRight();
		if (nextTile != null)
			result.add(nextTile);
	}

}