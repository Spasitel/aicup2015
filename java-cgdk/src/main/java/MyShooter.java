import java.util.LinkedHashSet;
import java.util.Set;

import model.Car;
import model.Game;
import model.World;

public class MyShooter {

	private static enum ShootResult {
		MISS, HIT, KILL;
	}

	public static boolean isDotInsidePlayerCard(MyVector dot, MyVector player, double angle, double playerHeight,
			double playerWidth) {
		MyVector[] carCorners = MyPlayerGeometry.playerCorners(player, angle, playerHeight, playerWidth);
		for (int i = 0; i < carCorners.length; ++i) {
			MyVector toNextCorner = carCorners[i].getVectorTo(carCorners[(i + 1) % carCorners.length]);
			MyVector fromCornerToDot = carCorners[i].getVectorTo(dot);
			if (toNextCorner.x * fromCornerToDot.y - toNextCorner.y * fromCornerToDot.x < 0.0)
				return false;
		}

		return true;
	}

	private static MyVector pointAtTime(MyVector start, MyVector velocity, double time) {
		return start.add(velocity.multiply(time));
	}

	private static double distAtTime(MyVector startCar, MyVector velCar, MyVector startBullet, MyVector velBullet,
			double time) {
		MyVector carAtTime = pointAtTime(startCar, velCar, time);
		MyVector velAtTime = pointAtTime(startBullet, velBullet, time);
		return carAtTime.distanceTo(velAtTime);
	}

	private static ShootResult predictHit(MyVector startPoint, MyVector fireDirection, Car enemyCar, boolean isJeep, Game game, int maxTick) {
		if (enemyCar.getDurability() < 0.009)
			return ShootResult.MISS;//TODO: mb he will restore

		if (enemyCar.isFinishedTrack())
			return ShootResult.MISS;

		MyVector enemyCenter = new MyVector(enemyCar.getX(), enemyCar.getY());
		MyVector enemyVel = new MyVector(enemyCar.getSpeedX(), enemyCar.getSpeedY());
		
		if (distAtTime(enemyCenter, enemyVel, startPoint, fireDirection, 0.0) > 100
				&& distAtTime(enemyCenter, enemyVel, startPoint, fireDirection, 1.0) > distAtTime(enemyCenter,
						enemyVel, startPoint, fireDirection, 0.0))
			return ShootResult.MISS;

		double left = 0.0, right = maxTick;

		while (left + 0.01 < right) {
			double m1 = left + (right - left) / 3;
			double m2 = left + 2 * (right - left) / 3;
			if (distAtTime(enemyCenter, enemyVel, startPoint, fireDirection, m1) > distAtTime(enemyCenter, enemyVel,
					startPoint, fireDirection, m2))
				left = m1;
			else
				right = m2;
		}

		MyVector bulletAtTime = pointAtTime(startPoint, fireDirection, left);
		MyVector enemyAtTime = pointAtTime(enemyCenter, enemyVel, left);

		if (!isDotInsidePlayerCard(bulletAtTime, enemyAtTime, enemyCar.getAngle(), enemyCar.getHeight(),
				enemyCar.getWidth()))
			return ShootResult.MISS;
		

		if (left < 50 && Math.abs(enemyCar.getAngularSpeed()) < 0.1 && enemyCar.getDurability() < 0.1501)
			return ShootResult.KILL;

		if (left < 15)
			return ShootResult.HIT;

		if (left < 50 && Math.abs(enemyCar.getAngularSpeed()) < 0.06)
			return ShootResult.HIT;

		return ShootResult.MISS;
	}

	public static boolean isShootBuggy(Car self, World world, Game game) {
		if (self.getRemainingProjectileCooldownTicks() > 0)
			return false;

		int minimumHitNumber;
		if (self.getProjectileCount() > 1 || world.getMyPlayer().getScore() > 1000) {
			if (self.getProjectileCount() > 1 && world.getMyPlayer().getScore() > 1000) {
				minimumHitNumber = 1;
			} else {
				minimumHitNumber = 2;
			}
		} else {
			minimumHitNumber = 3;
		}
		
		MyVector startPoint = new MyVector(self.getX(), self.getY());
		MyVector[] bullets = new MyVector[3];
		bullets[0] = new MyVector(60.0, 0).rotate(self.getAngle());
		bullets[1] = bullets[0].rotate(-Math.PI / 90);
		bullets[2] = bullets[0].rotate(Math.PI / 90);

		boolean isShoot = false;
		for (Car target : world.getCars()) {
			if (!target.isTeammate()) {
				isShoot |= isHitsEnought(minimumHitNumber, startPoint, bullets, target, game);
			}else if(!target.getType().equals(self.getType())){
				assert world.getPlayers().length == 2;
				if(isHitsEnought(1, startPoint, bullets, target, game))
					return false;
			}
		}

		return isShoot;
	}

	private static boolean isHitsEnought(int minimumHitNumber, MyVector startPoint, MyVector[] bullets, Car target, Game game) {
		int hitNumber = 0;

		for (MyVector bullet : bullets) {
			ShootResult result = predictHit(startPoint, bullet, target, false, game, 50);
			if (result.equals(ShootResult.KILL))
				return true;
			if (result.equals(ShootResult.HIT))
				hitNumber += 1;
		}

		if (hitNumber >= minimumHitNumber)
			return true;
		
		return false;
	}
	
	public static boolean isShootJeep(Car self, World world, Game game){
		if (self.getRemainingProjectileCooldownTicks() > 0)
			return false;
		
		MyVector startPoint = new MyVector(self.getX(), self.getY());
		MyVector bullet = new MyVector(60.0, 0).rotate(self.getAngle());
		boolean isShoot = false;

		
		int maxTick;
		for(maxTick = 0; maxTick < 40; maxTick++){
			if(isTireHitWall(bullet, startPoint, maxTick, game, world))
				break;
		}
		for (Car target : world.getCars()) {
			if (!target.isTeammate()) {
				ShootResult result = predictHit(startPoint, bullet, target, true, game, maxTick);
				isShoot |= result.equals(ShootResult.HIT) || result.equals(ShootResult.KILL);
			}else if(!target.getType().equals(self.getType())){
				assert world.getPlayers().length == 2;
				ShootResult result = predictHit(startPoint, bullet, target, true, game, maxTick);
				if(result.equals(ShootResult.HIT) || result.equals(ShootResult.KILL))
					return false;
			}
		}
		
		return isShoot;
	}

	private static final Set<MyVector> tireAngles = new LinkedHashSet<MyVector>(4);
	
	private static Set<MyVector> getTireAngles(Game game){
		if(tireAngles.isEmpty()){
			double rad = game.getTireRadius();
			tireAngles.add(new MyVector(rad, rad));
			tireAngles.add(new MyVector(-rad, rad));
			tireAngles.add(new MyVector(rad, -rad));
			tireAngles.add(new MyVector(-rad, -rad));
		}
		return tireAngles;
	}
	
	private static boolean isTireHitWall(MyVector bullet, MyVector startPoint, int maxTick, Game game, World world) {
		MyVector offset = bullet.multiply(maxTick).add(startPoint);
		for(MyVector angle:getTireAngles(game)){
			MyVector newPlace = angle.add(offset);
			if (MyWorldSimulator.isCollision(world, game, new MyWorldSimulator.Point(newPlace.x, newPlace.y)))
				return true;
		}
		return false;
	}

}