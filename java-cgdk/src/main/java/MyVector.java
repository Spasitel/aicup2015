import static java.lang.Math.*;

public class MyVector {

	private MyVector(double x, double y, double len, double angle) {
		super();
		this.x = x;
		this.y = y;
		this.len = len;
		this.angle = angle;
	}

	private final static double eps = 1e-7;
	public final double x, y;

	public MyVector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public MyVector add(MyVector other) {
		double x = this.x + other.x;
		double y = this.y + other.y;
		return new MyVector(x, y);
	}

	public MyVector subtract(MyVector other) {
		double x = this.x - other.x;
		double y = this.y - other.y;
		return new MyVector(x, y);
	}

	public MyVector multiply(double a) {
		double x = a * this.x;
		double y = a * this.y;
		if (angle > -8) {
			double angle2 = getAngle();
			if (a < 0)
				angle2 = MyMoveTrajectoryModeler.normalizeAngle(angle2 + Math.PI);
			return new MyVector(x, y, Math.abs(getLen() * a), angle2);
		} else
			return new MyVector(x, y);
	}

	public MyVector getVectorTo(MyVector to) {
		return to.subtract(this);
	}

	public double dotProduct(MyVector other) {
		return this.x * other.x + this.y * other.y;
	}

	private double len = -1;

	public double getLen() {
		if (len < 0) {
			len = Math.sqrt(x * x + y * y);
		}
		return len;
	}

	public double distanceTo(MyVector to) {
		return Math.sqrt((to.x - this.x) * (to.x - this.x) + (to.y - this.y) * (to.y - this.y));
	}

	public MyVector rotate(double deltaAngle) {
		double len = getLen();
		if (len < eps)
			return this;
		double angle = getAngle();
		double newAngle = MyMoveTrajectoryModeler.normalizeAngle(angle + deltaAngle);
		double x = len * cos(newAngle);
		double y = len * sin(newAngle);
		return new MyVector(x, y, len, newAngle);
	}

	private double angle = -10;

	public double getAngle() {
		if (angle < -8)
			angle = atan2(y, x);
		return angle;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
