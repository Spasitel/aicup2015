import java.util.LinkedHashSet;
import java.util.Set;

import model.Car;
import model.Game;

public enum MyMoveDecision {

//	BACK(false, 0.0, -1.0),
//	BACK_RIGHT(false, 1.0, -1.0),
//	BACK_LEFT(false, -1.0, -1.0),
	FORWARD(false, 0.0, 1.0),
	FORWARD_RIGHT(false, 1.0, 1.0),
	FORWARD_LEFT(false, -1.0, 1.0),
//	FORWARD_RIGHTER(false, 0.5, 1.0),
//	FORWARD_LEFTER(false, -0.5, 1.0),
	

//	BACK_BRAKE(true, 0.0, -1.0),
//	BACK_RIGHT_BRAKE(true, 1.0, -1.0),
//	BACK_LEFT_BRAKE(true, -1.0, -1.0),
	FORWARD_BRAKE(true, 0.0, 1.0),
	FORWARD_RIGHT_BRAKE(true, 1.0, 1.0),
	FORWARD_LEFT_BRAKE(true, -1.0, 1.0);

	private final MyDecision decision;

	MyMoveDecision(boolean brake, double wheelTurn, double enginePower) {
		decision = new MyDecision(brake, wheelTurn, enginePower);
	}

	public MyDecision getDecision() {
		return decision;
	}

	public static Set<MyMoveDecision> getDifferentDecisions(Car car, int steps, Game game){
		Set<MyMoveDecision> result = new LinkedHashSet<MyMoveDecision>();
//		boolean isAddBack = car.getRemainingNitroCooldownTicks() < steps;

//		if(car.getWheelTurn() > 0)
//			result.add(FORWARD_RIGHTER);
//		else
//			result.add(FORWARD_LEFTER);
		
		boolean isAddBreak = true;
		if(MyMoveExitFromBlock.isWasted(car))
			isAddBreak = false;
		
		result.add(FORWARD_RIGHT);
		result.add(FORWARD_LEFT);
		if(isAddBreak){
			result.add(FORWARD_RIGHT_BRAKE);
			result.add(FORWARD_LEFT_BRAKE);
		}

//		if(isAddBack){
//			result.add(BACK_RIGHT);
//			result.add(BACK_RIGHT_BRAKE);
//			result.add(BACK_LEFT);
//			result.add(BACK_LEFT_BRAKE);
//		}
		
		if(Math.abs(car.getWheelTurn()) < ((double)(steps)) * game.getCarWheelTurnChangePerTick()){
			result.add(FORWARD);
			if(isAddBreak)
				result.add(FORWARD_BRAKE);
//			if(isAddBack){
//				result.add(BACK);
//				result.add(BACK_BRAKE);
//			}
		}
		
		return result;
	}
}
