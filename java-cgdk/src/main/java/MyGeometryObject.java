public interface MyGeometryObject {
	public boolean intersect(MySegment other);
}
