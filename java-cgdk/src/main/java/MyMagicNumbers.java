public class MyMagicNumbers {

	public static int NEXT_TILES_TO_CALCULATE = 3;

	public static final int ASTAR_STEP_TILES = 20;

	public static final int STEPS_WASTED_FOR_BLOCK = 10;

	public static final int BACK_MOVE_DURATION_TIME = 40;

	public static final long BIG_TIME_LIMIT_MS = 100;

	public static final long SMALL_TIME_LIMIT_MS = 30;

}
