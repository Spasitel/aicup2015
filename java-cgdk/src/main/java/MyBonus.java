import java.util.LinkedHashSet;
import java.util.Set;

import model.BonusType;
import model.Car;
import model.Game;
import model.World;

public class MyBonus {

	public MyBonus(BonusType bonusType, double x, double y, MyTile tile) {
		this.bonusType = bonusType;
		this.x = x;
		this.y = y;
		this.tile = tile;
	}

	private final MyTile tile;
	private final BonusType bonusType;
	public final double x;
	public final double y;

	public BonusType getType() {
		return bonusType;
	}

	public MyTile getTile() {
		return tile;
	}

	private static double maxDistance2 = 0;
	private static double minDistance2 = 0;

	private Set<MyVector> cornersBonus = new LinkedHashSet<MyVector>(4);

	boolean isCollectedByCar(Car car, World world, Game game) {

		double carX = car.getX();
		double carY = car.getY();
		double carAngle = car.getAngle();

		return isCollected(game, carX, carY, carAngle);
	}

	private boolean isCollected(Game game, double carX, double carY, double carAngle) {
		double carHeight = game.getCarHeight();
		double carWidth = game.getCarWidth();

		double dX = carX - x;
		double dY = carY - y;
		if (maxDistance2 < 1.0)
			maxDistance2 = game.getBonusSize() * game.getBonusSize() / 2 + carHeight * carHeight / 4
					+ carWidth * carWidth / 4;

		double delta2 = dX * dX + dY * dY;
		if (delta2 > maxDistance2)
			return false;

		if (minDistance2 < 1.0)
			minDistance2 = game.getBonusSize() * game.getBonusSize() / 4 + carHeight * carHeight / 4;

		if (delta2 < minDistance2)
			return true;

		MyVector playerCenter = new MyVector(carX, carY);
		MyVector[] corners = MyPlayerGeometry.playerCorners(playerCenter, carAngle, carHeight,
				carWidth);
		boolean isMinX = true;
		boolean isMaxX = true;
		boolean isMinY = true;
		boolean isMaxY = true;
		double minX = x - game.getBonusSize() / 2;
		double maxX = x + game.getBonusSize() / 2;
		double minY = y - game.getBonusSize() / 2;
		double maxY = y + game.getBonusSize() / 2;

		for (MyVector corner : corners) {
			if (corner.x > minX) {
				if (corner.x < maxX)
					if (corner.y > minY)
						if (corner.y < maxY)
							return true;
				isMinX = false;
			}
			if (corner.x < maxX)
				isMaxX = false;
			if (corner.y > minY)
				isMinY = false;
			if (corner.y < maxY)
				isMaxY = false;
		}

		if (isMinX || isMaxX || isMinY || isMaxY)
			return false;

		if (cornersBonus.isEmpty()) {
			cornersBonus.add(new MyVector(minX, minY));
			cornersBonus.add(new MyVector(minX, maxY));
			cornersBonus.add(new MyVector(maxX, minY));
			cornersBonus.add(new MyVector(maxX, maxY));
		}

		for (MyVector cornerBonus : cornersBonus) {
			if (MyShooter.isDotInsidePlayerCard(cornerBonus, playerCenter, carAngle, carHeight,
					carWidth))
				return true;
		}

		return false;
	}

	public static Set<MyBonus> getCollectedBonuses(Set<MyBonus> alreadyCollected,
			Set<MyBonus> allBonuses, World world, Game game, double carX, double carY, double carAngle) {
		Set<MyBonus> result = new LinkedHashSet<MyBonus>();
		for(MyBonus bonus:allBonuses){
			if(alreadyCollected.contains(bonus))
				continue;
			if(bonus.isCollected(game, carX, carY, carAngle))
				result.add(bonus);
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bonusType == null) ? 0 : bonusType.hashCode());
		result = prime * result + ((tile == null) ? 0 : tile.hashCode());
		result = prime * result + (int) (x + 0.1);
		result = prime * result + (int) (y + 0.1);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyBonus other = (MyBonus) obj;
		if (bonusType != other.bonusType)
			return false;
		if (tile == null) {
			if (other.tile != null)
				return false;
		} else if (!tile.equals(other.tile))
			return false;
		if ((int) (x + 0.1) != (int) (other.x + 0.1))
			return false;
		if ((int) (y + 0.1 ) != (int) (other.y + 0.1))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MyBonus [tile=" + tile + ", bonusType=" + bonusType + ", x=" + x + ", y=" + y + ", cornersBonus="
				+ cornersBonus + ", hashCode()=" + hashCode() + "]";
	}


}
