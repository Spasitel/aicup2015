import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import model.Car;
import model.CarType;
import model.Game;
import model.Move;
import model.World;

public class MyMoveOptimalCalculator {
	/*
	 * Calculate optimal move cross NEXT_TILES_TO_CALCULATE next tails.
	 */
	static void calculateOptimalMove(Car self, World world, Game game, Set<List<MyTile>> pathes, Move move) {

		if (MyMoveExitFromBlock.isExitFromBlock(self, world, game, pathes, move))
			return;

		if (!MyStaticStorage.isPanicMod(world)) {
			MyEvaluationCalculator evaluationCalculator = MyEvaluationCalculator.createEvaluationCalculator(self,
					world, game, pathes);
			CurrentAStarState resultState = getBestDecisionFromAStar(self, world, game, pathes, evaluationCalculator);

			if (resultState != null) {
				MyDecision decision = resultState.getFirstDecision().getDecision().copy();
				if (resultState.isMayUseNitro())
					decision.setUseNitro(true);
				MyStaticStorage.setBigTimeLimit(self.getType(), true);

				if (evaluationCalculator.getBeforeTurn() != null && evaluationCalculator.getBeforeTurn().bestPlace != 0
						&& !decision.isBrake() && resultState.getCollectedBonuses().isEmpty()) {
					MyDecision gotoBestPlace = gotoBestPlace(pathes, self, evaluationCalculator.getBeforeTurn(), game,
							world);
					if (gotoBestPlace != null) {
						move.setBrake(gotoBestPlace.isBrake());
						move.setEnginePower(gotoBestPlace.getEnginePower());
						move.setWheelTurn(gotoBestPlace.getWheelTurn());
						if (Math.abs(self.getWheelTurn()) < 0.25 && Math.abs(self.getAngularSpeed()) < 0.05
								&& self.getRemainingOiledTicks() == 0)
							move.setUseNitro(decision.isUseNitro());

						return;
					}
				}

				move.setBrake(decision.isBrake());
				move.setEnginePower(decision.getEnginePower());
				move.setWheelTurn(decision.getWheelTurn());
				if (Math.abs(self.getWheelTurn()) < 0.25 && Math.abs(self.getAngularSpeed()) < 0.05
						&& self.getRemainingOiledTicks() == 0)
					move.setUseNitro(decision.isUseNitro());
				return;
			}
		}

		MyStrategy.debug(1, "Make dumb move:" + world.getTick());
		MyMoveDumb.makeDumbMove(self, world, game, pathes, move, false);

	}

	/*
	 * AStar algorithm
	 */
	private static CurrentAStarState getBestDecisionFromAStar(Car self, World world, Game game,
			Set<List<MyTile>> pathes, MyEvaluationCalculator evaluationCalculator) {
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());

		TreeSet<CurrentAStarState> toWatch = new TreeSet<MyMoveOptimalCalculator.CurrentAStarState>();
		CarType carType = self.getType();
		MyMoveTrajectoryModeler trajectoryModeler = new MyMoveTrajectoryModeler(carType, world, game);

		// Initial state
		MyStaticStorage.SavedStateFromAStar savedState = MyStaticStorage.getSavedState(carType);
		MyMoveDecision savedDecision = null;
		if (savedState != null && world.getTick() < savedState.getEndTickNumber()) {
			Car savedCar = savedState.getPredictedState();
			if (isCarSame(self, savedCar)) {
				if (selfTile.equals(savedState.getStartTile())) {
					Set<MyBonus> collectedBonuses = evaluationCalculator.adaptateBonuses(savedState
							.getCollectedBonuses()); // From saved state
					MyMoveModelResult modelTrajectory = trajectoryModeler.modelTrajectory(self, savedState
							.getPrevDecision().getDecision(), savedState.getEndTickNumber() - world.getTick(),
							savedState.getPath(), 0, collectedBonuses, evaluationCalculator.getAllBonuses());

					if (modelTrajectory != null) {
						savedDecision = savedState.getPrevDecision();
						int stateEvaluation = evaluationCalculator.getEvaluation(savedState.getEndState(), world, game);
						int stateEstimation = evaluationCalculator.getEstimation(savedState.getEndState(), world, game,
								collectedBonuses, pathes);
						CurrentAStarState aStarState = new CurrentAStarState(stateEvaluation + stateEstimation
								+ savedState.getGoalReachedTickNumber() - world.getTick(), self/* TODO */,
								savedState.getGoalReachedTickNumber() - world.getTick(), savedDecision,
								savedState.getPath(), true, savedState.getPath().size() - 1, false, collectedBonuses);
						toWatch.add(aStarState);

						// TODO: Create AStarState and add to toWatch
						// Car predictedState = modelTrajectory.getCar();
						// MyStaticStorage.getSavedState(carType).setPredictedState(predictedState);
						// return savedDecision.getDecision();// repeat last
					}
				}
			} else {

				String str = "";
				str += "Collision! Tick: " + world.getTick();
				if (isNear(self.getAngle(), savedCar.getAngle()))
					str += " angel " + (self.getAngle() - savedCar.getAngle()) + "; ";
				if (!isNear(self.getX(), savedCar.getX()))
					str += " X " + (self.getX() - savedCar.getX()) + "; ";
				if (!isNear(self.getY(), savedCar.getY()))
					str += " Y " + (self.getY() - savedCar.getY()) + "; ";
				if (!isNear(self.getSpeedX(), savedCar.getSpeedX()))
					str += " speedX " + (self.getSpeedX() - savedCar.getSpeedX()) + "; ";
				if (!isNear(self.getSpeedY(), savedCar.getSpeedY()))
					str += " speedY " + (self.getSpeedY() - savedCar.getSpeedY()) + "; ";

				MyStrategy.debug(2, str);
			}
		}

		int steps = 0;
		for (MyMoveDecision moveDecision : MyMoveDecision.getDifferentDecisions(self, MyMagicNumbers.ASTAR_STEP_TILES,
				game)) {

			if (!moveDecision.equals(savedDecision)) {
				MyDecision decision = moveDecision.getDecision();
				MyMoveModelResult modelResult = trajectoryModeler.modelTrajectory(self, decision,
						MyMagicNumbers.ASTAR_STEP_TILES, pathes, 0, new LinkedHashSet<MyBonus>(),
						evaluationCalculator.getAllBonuses());

				if (modelResult != null) {
					Car nextState = modelResult.getCar();
					if (!MyWorldSimulator.isCollision(nextState, world, game)) {

						Set<MyBonus> collectedBonuses = modelResult.getCollectedBonuses();
						int estimate = evaluationCalculator.getEstimation(nextState, world, game, collectedBonuses,
								pathes);
						boolean isMayNitro = moveDecision.equals(MyMoveDecision.FORWARD);
						int evaluation = 0;
						if (modelResult.isEndReached()) {
							evaluation = evaluationCalculator.getEvaluation(nextState, world, game);
						}
						CurrentAStarState aStarState = new CurrentAStarState(evaluation + estimate
								+ modelResult.getStepsModeled(), nextState, modelResult.getStepsModeled(),
								moveDecision, modelResult.getPaths(), modelResult.isEndReached(),
								modelResult.getStepInPath(), isMayNitro, collectedBonuses);
						toWatch.add(aStarState);
					}
				}
			}
		}

		// AStar
		while (!toWatch.isEmpty()) {
			steps++;
			if (!MyStrategy.ignoreTimeLimit) {
				if (steps % 10 == 0 && isTimeLimitReached(self.getType())) {
					MyStaticStorage.setBigTimeLimit(self.getType(), false);
					MyStrategy.debug(-2, world.getTick() + " steps: " + steps);
					return null;// Timeout
				}
			} else {
				if (steps > 150) {
					MyStrategy.debug(-2, "OVERSTEPS: " + world.getTick() + " steps: " + steps);
					return null;
				}
			}

			CurrentAStarState currentAStarState = toWatch.iterator().next();
			if (currentAStarState.isEndReach()) {
				MyMoveDecision moveDecision = currentAStarState.getFirstDecision();
				MyDecision decision = moveDecision.getDecision();
				Car predictedState = trajectoryModeler.modelTrajectory(self, decision, 1, currentAStarState.getPaths(),
						0, currentAStarState.getCollectedBonuses(), evaluationCalculator.getAllBonuses()).getCar();
				if (moveDecision.equals(savedDecision)) {
					MyStaticStorage.getSavedState(carType).setPredictedState(predictedState);
					MyStaticStorage.getSavedState(carType).setCollectedBonuses(currentAStarState.getCollectedBonuses());
					MyStrategy.debug(2, "Tick " + world.getTick() + " used prev state");
					return currentAStarState;// ???
				}
				int endTickNumber = world.getTick() + MyMagicNumbers.ASTAR_STEP_TILES;
				int goalReachedTickNumber = world.getTick() + currentAStarState.getCurrentTicks();
				MyStaticStorage.SavedStateFromAStar state = new MyStaticStorage.SavedStateFromAStar(predictedState, moveDecision,
						endTickNumber, goalReachedTickNumber, selfTile, currentAStarState.getCar(),
						currentAStarState.getPaths(), currentAStarState.getCollectedBonuses());
				MyStaticStorage.setSavedState(carType, state);
				return currentAStarState;// Calculated decision
			}
			toWatch.remove(currentAStarState);

			for (MyMoveDecision moveDecision : MyMoveDecision.getDifferentDecisions(currentAStarState.getCar(),
					MyMagicNumbers.ASTAR_STEP_TILES, game)) {
				MyMoveModelResult modelResult = trajectoryModeler.modelTrajectory(currentAStarState.getCar(),
						moveDecision.getDecision(), MyMagicNumbers.ASTAR_STEP_TILES, currentAStarState.getPaths(),
						currentAStarState.getStepInPath(), currentAStarState.getCollectedBonuses(),
						evaluationCalculator.getAllBonuses());
				if (modelResult != null) {
					Car nextState = modelResult.getCar();
					if (!MyWorldSimulator.isCollision(nextState, world, game)) {

						Set<MyBonus> collectedBonuses = modelResult.getCollectedBonuses();
						int estimate = evaluationCalculator.getEstimation(nextState, world, game, collectedBonuses,
								modelResult.getPaths());
						int estimateNext = estimate + currentAStarState.getCurrentTicks()
								+ modelResult.getStepsModeled();
						if (estimateNext < currentAStarState.getEstimate()) {
							MyStrategy.debug(2, "Fail with estimate: " + world.getTick());
						}

						boolean isMayNitro = currentAStarState.isMayUseNitro()
								&& moveDecision.equals(MyMoveDecision.FORWARD);
						int evaluation = 0;
						if (modelResult.isEndReached())
							evaluation = evaluationCalculator.getEvaluation(nextState, world, game);
						CurrentAStarState aStarState = new CurrentAStarState(evaluation + estimateNext, nextState,
								modelResult.getStepsModeled() + currentAStarState.getCurrentTicks(),
								currentAStarState.getFirstDecision(), modelResult.getPaths(),
								modelResult.isEndReached(), modelResult.getStepInPath(), isMayNitro, collectedBonuses);
						toWatch.add(aStarState);
					}
				}
			}

		}

		MyStrategy.debug(1, "No path find: " + world.getTick());
		return null;// path unreachable
	}

	// if we go forward to corner, turn to best place
	private static MyDecision gotoBestPlace(Set<List<MyTile>> pathes, Car self,
			MyEvaluationBeforeTurn dataForEvaluation, Game game, World world) {
		// check path
		List<MyTile> path = new LinkedList<MyTile>(pathes.iterator().next());
		if (path.size() > 2 && MyPathGenerator.is180TurnAfterPath(path, self, world, game))
			path.remove(2);
		MyTile lastTile = path.get(path.size() - 1);
		int changeX = lastTile.x - path.get(path.size() - 2).x;
		int changeY = lastTile.y - path.get(path.size() - 2).y;
		for (int i = path.size() - 2; i > 0; i--) {
			if (path.get(i).x - path.get(i - 1).x != changeX)
				return null;
			if (path.get(i).y - path.get(i - 1).y != changeY)
				return null;
		}
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());
		if (path.get(0).x - selfTile.x != changeX)
			return null;
		if (path.get(0).y - selfTile.y != changeY)
			return null;

		// check position in tile
		if (dataForEvaluation.isX) {
			if (Math.abs(selfTile.x * game.getTrackTileSize() + game.getTrackTileSize() / 2 - self.getX()) > game
					.getTrackTileSize() / 2 - game.getTrackTileMargin())
				return null;
		} else {
			if (Math.abs(selfTile.y * game.getTrackTileSize() + game.getTrackTileSize() / 2 - self.getY()) > game
					.getTrackTileSize() / 2 - game.getTrackTileMargin())
				return null;
		}

		// check direction of velocity
		double speedX = self.getSpeedX();
		double speedY = self.getSpeedY();
		if (changeX != 0) {
			if (changeX * speedX < 0)
				return null;
		} else {
			if (Math.abs(speedX) * 2 > Math.abs(speedY))
				return null;
		}

		if (changeY != 0) {
			if (changeY * speedY < 0)
				return null;
		} else {
			if (Math.abs(speedY) * 2 > Math.abs(speedX))
				return null;
		}

		// check car angle and velocity angle
		MyVector vel = new MyVector(speedX, speedY);
		if (Math.abs(MyMoveTrajectoryModeler.normalizeAngle(self.getAngle() - vel.getAngle())) > 0.1)
			return null;

		// check wheel turn
		if (Math.abs(self.getWheelTurn()) > 11 / vel.getLen() - game.getCarWheelTurnChangePerTick())
			return null;

		// calculate right or left
		double delta = 10;// TODO:
		boolean isRight;
		if (dataForEvaluation.isX) {
			double time;
			if (speedY > 0)
				time = (lastTile.y * game.getTrackTileSize() - self.getY()) / speedY;
			else
				time = ((lastTile.y + 1) * game.getTrackTileSize() - self.getY()) / speedY;
			assert time > 0;
			double changeSpeedX = -Math.abs(speedY) * speedY * game.getCarAngularSpeedFactor() * self.getWheelTurn()
					/ 2;
			double newX = self.getX() + (speedX) * time + changeSpeedX * time * time / 2;

			double deltaX = dataForEvaluation.bestPlace - newX;
			double dX = dataForEvaluation.bestPlace - self.getX();
			if (Math.abs(deltaX) < delta)
				return null;
			if (dX * deltaX < 0 && deltaX / dX > -2.0)
				return null;
			if (deltaX * speedY > 0)
				isRight = false;
			else
				isRight = true;
		} else {
			double time;
			if (speedX > 0)
				time = (lastTile.x * game.getTrackTileSize() - self.getX()) / speedX;
			else
				time = ((lastTile.x + 1) * game.getTrackTileSize() - self.getX()) / speedX;
			assert time > 0;
			double changeSpeedY = Math.abs(speedX) * speedX * game.getCarAngularSpeedFactor() * self.getWheelTurn() / 2;
			double newY = self.getY() + (speedY) * time + changeSpeedY * time * time / 2;
			double deltaY = dataForEvaluation.bestPlace - newY;
			double dY = dataForEvaluation.bestPlace - self.getY();
			if (Math.abs(deltaY) < delta)
				return null;
			if (dY * deltaY < 0 && deltaY / dY > -2.0)
				return null;
			if (deltaY * speedX > 0)
				isRight = true;
			else
				isRight = false;

		}

		// set wheel turn and clean saved state
		if (isRight)
			return new MyDecision(false, 1.0, 1.0);
		else
			return new MyDecision(false, -1.0, 1.0);
	}

	private static boolean isTimeLimitReached(CarType type) {
		long time = System.currentTimeMillis() - MyStrategy.startTime;

		boolean result;
		if (MyStaticStorage.isBigTimeLimit(type)) {
			result = time > MyMagicNumbers.BIG_TIME_LIMIT_MS;
			if (result)
				MyStrategy.debug(-2, "TIMEOUT BIG: ");
		} else {
			result = time > MyMagicNumbers.SMALL_TIME_LIMIT_MS;
			if (result)
				MyStrategy.debug(-2, "TIMEOUT SMALL: ");
		}

		return result;
	}

	private static boolean isCarSame(Car self, Car savedCar) {
		if (isNear(self.getAngle(), savedCar.getAngle()) && isNear(self.getX(), savedCar.getX())
				&& isNear(self.getY(), savedCar.getY()) && isNear(self.getSpeedX(), savedCar.getSpeedX())
				&& isNear(self.getSpeedY(), savedCar.getSpeedY()))
			return true;
		return false;
	}

	private static boolean isNear(double angle, double angle2) {
		return Math.abs(angle - angle2) / (Math.abs(angle) + Math.abs(angle2)) < 0.01
				|| Math.abs(angle - angle2) < 0.01;
	}

	private static class CurrentAStarState implements Comparable<CurrentAStarState> {

		private final int estimate;
		private final Car car;
		private final int currentTicks;
		private final MyMoveDecision firstDecision;
		private final Set<List<MyTile>> paths;
		private final boolean isEndReach;
		private final int stepInPath;
		private final boolean isMayUseNitro;
		private final Set<MyBonus> collectedBonuses;

		public CurrentAStarState(int estimate, Car car, int currentTicks, MyMoveDecision firstDecision,
				Set<List<MyTile>> paths, boolean isEndReach, int stepInPath, boolean isMayUseNitro,
				Set<MyBonus> collectedBonuses) {
			super();
			this.car = car;
			this.currentTicks = currentTicks;
			this.firstDecision = firstDecision;
			this.estimate = estimate;
			this.paths = paths;
			this.isEndReach = isEndReach;
			this.stepInPath = stepInPath;
			this.isMayUseNitro = isMayUseNitro;
			this.collectedBonuses = collectedBonuses;
		}

		public int getStepInPath() {
			return stepInPath;
		}

		public int getEstimate() {
			return estimate;
		}

		public Car getCar() {
			return car;
		}

		public int getCurrentTicks() {
			return currentTicks;
		}

		public MyMoveDecision getFirstDecision() {
			return firstDecision;
		}

		public Set<List<MyTile>> getPaths() {
			return paths;
		}

		public boolean isEndReach() {
			return isEndReach;
		}

		@Override
		public int compareTo(CurrentAStarState arg0) {
			if (estimate - arg0.estimate != 0)
				return estimate - arg0.estimate; // Min - better

			if (currentTicks - arg0.currentTicks != 0)
				return arg0.currentTicks - currentTicks; // Max - better

			if (!firstDecision.equals(arg0.firstDecision))
				return firstDecision.compareTo(arg0.firstDecision);

			if (car != arg0.car)
				return car.hashCode() - arg0.car.hashCode();

			return 0;
		}

		public boolean isMayUseNitro() {
			return isMayUseNitro;
		}

		@Override
		public String toString() {
			return "CurrentAStarState [currentTicks=" + currentTicks + ", firstDecision=" + firstDecision
					+ ", estimate=" + estimate + ", isEndReach=" + isEndReach + ", stepInPath=" + stepInPath
					+ ", isMayUseNitro=" + isMayUseNitro + "]";
		}

		public Set<MyBonus> getCollectedBonuses() {
			return collectedBonuses;
		}

	}
}
