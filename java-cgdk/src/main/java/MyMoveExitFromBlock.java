import java.util.List;
import java.util.Set;

import model.Car;
import model.Game;
import model.Move;
import model.World;

public class MyMoveExitFromBlock {

	public static boolean isExitFromBlock(Car self, World world, Game game, Set<List<MyTile>> pathes, Move move) {

		SavedBlockState blockState = MyStaticStorage.getBlockState(self.getType());

		switch (blockState.currentState) {
		case NOT_BLOCK:
			return doNotBlockMove(self, blockState, world, game, pathes, move);
		case NOT_GO_FORWARD:
			return doNotGoForwardMove(self, blockState, world, game, pathes, move);
		case MOVE_BACK:
			return doMoveBackMove(self, blockState, world, game, pathes, move);
		case NOT_GO_FAST_BACKWARD:
			return doNotGoFastBackwardMove(self, blockState, world, game, pathes, move);
		case PREPARE_TO_GO:
			return doPrepareToGoMove(self, blockState, world, game, pathes, move);
		default:
			assert false;
			return false;
		}

		// if (MyExitFromBlockMover.isWasted(self)) {
		// MyStaticStorage.incrementWastedtime(self.getType());
		// if (MyStaticStorage.getWastedtime(self.getType()) > MyMagicNumbers.STEPS_WASTED_FOR_BLOCK) {
		// MyStaticStorage.setStartBackMoveDuration(self.getType());
		// }
		// } else {
		// MyStaticStorage.clearWastedTime(self.getType());
		// }
		//
		// if (MyStaticStorage.isBackMoveDuration(self.getType())) {
		// MyDumbMover.makeDumbMove(self, world, game, pathes, move, true);
		// MyStaticStorage.decreaseBackMoveDuration(self.getType());
		// MyStaticStorage.setBigTimeLimit(self.getType(), false);
		// return true;
		// }
		//
		// return false;
	}

	private static boolean doNotBlockMove(Car self, SavedBlockState blockState, World world, Game game,
			Set<List<MyTile>> pathes, Move move) {
		if (MyMoveExitFromBlock.isWasted(self)) {
			int wasted = blockState.currentWasteCount + 1;
			if (wasted > MyMagicNumbers.STEPS_WASTED_FOR_BLOCK) {
				SavedBlockState newState = new SavedBlockState(ExitBlockState.NOT_GO_FORWARD, 0, 0, wasted);
				return doNotGoForwardMove(self, newState, world, game, pathes, move);
			} else {
				SavedBlockState newState = new SavedBlockState(ExitBlockState.NOT_BLOCK, 0, 0, wasted);
				MyStaticStorage.setBlockState(self.getType(), newState);
				return false;
			}
		} else {
			MyStaticStorage.clearBlockState(self.getType());
			return false;
		}
	}

	private static boolean doNotGoForwardMove(Car self, SavedBlockState blockState, World world, Game game,
			Set<List<MyTile>> pathes, Move move) {
		if(self.getEnginePower() < 0){
			int endCurrStateTick = world.getTick() + MyMagicNumbers.BACK_MOVE_DURATION_TIME;
			SavedBlockState newState = new SavedBlockState(ExitBlockState.MOVE_BACK, endCurrStateTick , 0, 0);
			return doMoveBackMove(self, newState , world, game, pathes, move);
		}
		
		if (MyMoveExitFromBlock.isWasted(self)) {
			MyMoveDumb.makeDumbMove(self, world, game, pathes, move, true);
			move.setBrake(false);
			MyStaticStorage.setBlockState(self.getType(), blockState);
			return true;
		} else {
			MyStaticStorage.clearBlockState(self.getType());
			return false;
		}
	}

	private static boolean doMoveBackMove(Car self, SavedBlockState blockState, World world, Game game,
			Set<List<MyTile>> pathes, Move move) {
		if(world.getTick() > blockState.endCurrStateTick){
			SavedBlockState newState = new SavedBlockState(ExitBlockState.NOT_GO_FAST_BACKWARD, 0, 0, 0);
			return doNotGoFastBackwardMove(self, newState , world, game, pathes, move);
		}else{
			MyMoveDumb.makeDumbMove(self, world, game, pathes, move, true);
			move.setBrake(false);
			MyStaticStorage.setBlockState(self.getType(), blockState);
			return true;
		}
	}

	private static boolean doNotGoFastBackwardMove(Car self, SavedBlockState blockState, World world, Game game,
			Set<List<MyTile>> pathes, Move move) {
		if(self.getEnginePower() > -0.5){
			SavedBlockState newState = new SavedBlockState(ExitBlockState.PREPARE_TO_GO, 0, 0, 0);
			return doPrepareToGoMove(self, newState , world, game, pathes, move);
		}else{
			MyMoveDumb.makeDumbMove(self, world, game, pathes, move, true);
			move.setBrake(false);
			move.setEnginePower(1.0);
			MyStaticStorage.setBlockState(self.getType(), blockState);
			return true;
			
		}
	}

	private static boolean doPrepareToGoMove(Car self, SavedBlockState blockState, World world, Game game,
			Set<List<MyTile>> pathes, Move move) {
		if(self.getEnginePower() > 0){
			SavedBlockState newState = SavedBlockState.createEmpty();
			return doNotBlockMove(self, newState , world, game, pathes, move);
		}else{
			MyMoveDumb.makeDumbMove(self, world, game, pathes, move, false);
			move.setBrake(false);
			MyStaticStorage.setBlockState(self.getType(), blockState);
			return true;
		}
	}

	public static boolean isWasted(Car self) {
		return MyMoveExitFromBlock.isSmall(self.getSpeedX()) && MyMoveExitFromBlock.isSmall(self.getSpeedY())
				&& MyMoveExitFromBlock.isSmall(self.getAngularSpeed());
	}

	private static boolean isSmall(double speedX) {
		return Math.abs(speedX) < MyMoveExitFromBlock.wastedDelta;
	}

	private final static double wastedDelta = 0.3;

	public enum ExitBlockState {
		NOT_BLOCK, NOT_GO_FORWARD, MOVE_BACK, NOT_GO_FAST_BACKWARD, PREPARE_TO_GO;
	}

	public static class SavedBlockState {
		final ExitBlockState currentState;
		final int endCurrStateTick;
		final double whellTurn;
		final int currentWasteCount;

		public SavedBlockState(ExitBlockState currentState, int endCurrStateTick, double whellTurn,
				int currentWasteCount) {
			super();
			this.currentState = currentState;
			this.endCurrStateTick = endCurrStateTick;
			this.whellTurn = whellTurn;
			this.currentWasteCount = currentWasteCount;
		}

		public static SavedBlockState createEmpty() {
			return new SavedBlockState(ExitBlockState.NOT_BLOCK, 0, 0, 0);
		}

		@Override
		public String toString() {
			return "SavedBlockState [currentState=" + currentState + ", endCurrStateTick=" + endCurrStateTick
					+ ", whellTurn=" + whellTurn + ", currentWasteCount=" + currentWasteCount + "]";
		}
	}
}
