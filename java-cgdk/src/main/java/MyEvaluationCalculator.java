import java.util.List;
import java.util.Set;

import model.Car;
import model.Game;
import model.World;

public class MyEvaluationCalculator {

	public MyEvaluationCalculator(MyEvaluationBeforeTurn beforeTurn, MyEvaluationBonuses evaluationBonuses) {
		super();
		this.beforeTurn = beforeTurn;
		this.evaluationBonuses = evaluationBonuses;
	}

	private final MyEvaluationBeforeTurn beforeTurn;
	private final MyEvaluationBonuses evaluationBonuses;
	
	public static MyEvaluationCalculator createEvaluationCalculator(Car self, World world, Game game, Set<List<MyTile>> pathes){
		MyEvaluationBeforeTurn turn = getDataForEvaluation(self, world, game, pathes);
		MyEvaluationBonuses bonuses = MyEvaluationBonuses.create(self,world, game, pathes);
		return new MyEvaluationCalculator(turn, bonuses);
	}

	private static MyEvaluationBeforeTurn getDataForEvaluation(Car self, World world, Game game, Set<List<MyTile>> pathes) {
		if (pathes.size() > 1)
			return null;
		MyTile selfTile = MyTile.getTile(self.getX(), self.getY());
		return MyEvaluationBeforeTurn.create(self, world, game, pathes, selfTile);
	}
	
	public int getEstimation(Car endState, World world, Game game, Set<MyBonus> collectedBonuses, Set<List<MyTile>> pathes){
		int result = 0;
		result += MyEvaluationPath.getEstimationFromPath(endState, pathes, game, world);
		result += evaluationBonuses.getStateEvaluation(endState,world,game, collectedBonuses);
		return result;
	}
	
	public int getEvaluation(Car endState, World world, Game game){
		int result = 0;
		if(beforeTurn != null)
			result += beforeTurn.getStateEvaluation(endState, world, game);
		return result;
	}

	public MyEvaluationBeforeTurn getBeforeTurn() {
		return beforeTurn;
	}

	public Set<MyBonus> adaptateBonuses(Set<MyBonus> collectedBonuses) {
		return evaluationBonuses.adaptateBonuses(collectedBonuses);
	}

	public Set<MyBonus> getAllBonuses() {
		return evaluationBonuses.getAllBonuses();
	}
}
