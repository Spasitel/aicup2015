import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import model.Car;
import model.Game;
import model.World;


public class MyEvaluationBeforeTurn {

	final double placeRatio;
	final double bestPlace;
	final boolean isX;
	final int distance;

	public MyEvaluationBeforeTurn(double placeRatio, double bestPlace, boolean isX, int distance) {
		super();
		this.placeRatio = placeRatio;
		this.bestPlace = bestPlace;
		this.isX = isX;
		this.distance = distance;
	}

	static MyEvaluationBeforeTurn create(Car self, World world, Game game, Set<List<MyTile>> pathes,
			MyTile startTile) {
		assert pathes.size() == 1;
		List<MyTile> path = new LinkedList<MyTile>(pathes.iterator().next());
		if(path.size()>2 && MyPathGenerator.is180TurnAfterPath(path, self, world, game))
			path.remove(2);
		assert path.size() >= 2;
		MyTile lastTile = path.get(path.size() - 1);
		MyTile prevTile = path.get(path.size() - 2);
		boolean newIsX = prevTile.equals(lastTile.getBot()) || prevTile.equals(lastTile.getTop());
		boolean isIncrease = prevTile.equals(lastTile.getTop()) || prevTile.equals(lastTile.getLeft());
		MyTile prevPrevTile;
		if (path.size() == 2)
			prevPrevTile = startTile;
		else
			prevPrevTile = path.get(path.size() - 3);

		boolean isOnOneLine;
		int changeX = 0;
		int changeY = 0;
		if (newIsX) {
			if (isIncrease) {
				isOnOneLine = prevPrevTile.equals(prevTile.getTop());
				changeY = 1;
			} else {
				isOnOneLine = prevPrevTile.equals(prevTile.getBot());
				changeY = -1;
			}
		} else {
			if (isIncrease) {
				isOnOneLine = prevPrevTile.equals(prevTile.getLeft());
				changeX = 1;
			} else {
				isOnOneLine = prevPrevTile.equals(prevTile.getRight());
				changeX = -1;
			}
		}

		if (!isOnOneLine) {
			return new MyEvaluationBeforeTurn(0, 0, newIsX, 0);
		}

		int nextWaypointIndex = self.getNextWaypointIndex();
		for (MyTile tile : path) {
			if (tile.x == world.getWaypoints()[nextWaypointIndex][0]
					&& tile.y == world.getWaypoints()[nextWaypointIndex][1])
				nextWaypointIndex = (nextWaypointIndex + 1) % world.getWaypoints().length;
		}

		int x = lastTile.x;
		int y = lastTile.y;
		int[][] bfsForNextWaypoint = null;
		waypoint: while (true) {
			MyTile nextWaypointTile = MyTile.tiles[world.getWaypoints()[nextWaypointIndex][0]][world.getWaypoints()[nextWaypointIndex][1]];
			bfsForNextWaypoint = MyPathGenerator.getSavedBFSForWaypoint(world, nextWaypointTile);
			do {
				int value = bfsForNextWaypoint[x][y];
				if (value == 0) {
					nextWaypointIndex = (nextWaypointIndex + 1) % world.getWaypoints().length;
					continue waypoint;
				}

				int newX = x + changeX;
				int newY = y + changeY;
				if (newX < 0 || newX >= world.getWidth() || newY < 0
						|| newY >= world.getHeight()) {
					break waypoint;
				}
				if (bfsForNextWaypoint[newX][newY] == value - 1) {
					x = newX;
					y = newY;
				} else {
					break waypoint;
				}

			} while (true);
		}

		int newDistance = Math.abs((lastTile.x - x) + (lastTile.y - y));

		MyTile corner = MyTile.tiles[x][y];
		int valueCorner = bfsForNextWaypoint[x][y];
		boolean isLeft = false;
		boolean isRight = false;
		for (MyTile connect : corner.getConnectedTiles(world)) {
			if (bfsForNextWaypoint[connect.x][connect.y] == valueCorner - 1) {
				if (x - changeY == connect.x && y + changeX == connect.y)
					isRight = true;
				if (x + changeY == connect.x && y - changeX == connect.y)
					isLeft = true;
			}
		}

		if (!(isRight ^ isLeft))
			return new MyEvaluationBeforeTurn(0, 0, newIsX, 0);

		double newPlaceRatio = 0.02;
		double magicConstant = 600;
		double newBestPlace;
		if (isRight) {
			if (newIsX) {
				newBestPlace = lastTile.x * game.getTrackTileSize();
				if (isIncrease) {
					newBestPlace += magicConstant;
				} else {
					newBestPlace += game.getTrackTileSize() - magicConstant;
				}
			} else {
				newBestPlace = lastTile.y * game.getTrackTileSize();
				if (isIncrease) {
					newBestPlace += game.getTrackTileSize() - magicConstant;
				} else {
					newBestPlace += magicConstant;
				}
			}
		} else {
			if (newIsX) {
				newBestPlace = lastTile.x * game.getTrackTileSize();
				if (isIncrease) {
					newBestPlace += game.getTrackTileSize() - magicConstant;

				} else {
					newBestPlace += magicConstant;

				}
			} else {
				newBestPlace = lastTile.y * game.getTrackTileSize();
				if (isIncrease) {
					newBestPlace += magicConstant;

				} else {
					newBestPlace += game.getTrackTileSize() - magicConstant;

				}
			}
		}
		if(newDistance == 0){
			MyStaticStorage.setSpillOilTile(self.getType(), new MyStaticStorage.SpillOilData(prevPrevTile, prevTile, newIsX, newBestPlace));
		}
		return new MyEvaluationBeforeTurn(newPlaceRatio, newBestPlace, newIsX, newDistance);
	}


	public int getStateEvaluation(Car endState, World world, Game game) {
		if (isX)
			return (int) (placeRatio * Math.abs(endState.getX() - bestPlace));
		else
			return (int) (placeRatio * Math.abs(endState.getY() - bestPlace));
	}

}