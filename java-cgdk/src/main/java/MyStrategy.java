import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import model.Car;
import model.CarType;
import model.Game;
import model.Move;
import model.World;

public final class MyStrategy implements Strategy {
	public static boolean ignoreTimeLimit = false;
	private static boolean test = false;
	private static boolean debug = false;
	private static int debuglvl = -1;
	private static boolean init = false;
	public static long startTime = 0;

	private static PrintWriter writer;

	@Override
	public void move(Car self, World world, Game game, Move move) {
		startTime = System.currentTimeMillis();

		doMove(self, world, game, move);
		debug(10, "MOVE: " + move.toString());
		if (debug && writer != null)
			writer.flush();

		long endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		MyStaticStorage.totalTime += time;

		if (time > 15)
			debug(0, "TimeOut " + world.getTick() + " " + time + " remaining time: "
					+ (5000 + 15 * world.getTick() - MyStaticStorage.totalTime));

		if (MyStaticStorage.isPanicMod(world))
			debug(-10, "PANIC " + world.getTick());

	}

	public void doMove(Car self, World world, Game game, Move move) {
		if (!init) {
			init(world);
		}

		debug(10, "NEW TICK: " + world.getTick());
		debug(10, "CAR: " + self.toString());

		MyTile.initWorld(self, game, world);

		if (world.getTick() < game.getInitialFreezeDurationTicks()) {
			move.setEnginePower(1.0);
			debug(10, "START!");
			return;
		}

		if (debug && test) {
			if (world.getTick() < 300) {
				move.setEnginePower(1.0);
				// move.setUseNitro(true);
				move.setWheelTurn(0.0);
				return;
			}
			if (world.getTick() >= 300) {
				if (self.getSpeedY() > -0.01)
					System.exit(0);
				move.setEnginePower(0.0);
				if (self.getEnginePower() < 0.01)
					// move.setBrake(true);
					// else
					move.setWheelTurn(1.0);
				return;
			}
		}

		if (self.isFinishedTrack()) {
			debug(10, "FINISH!");
			if (debug && writer != null) {
				if (world.getPlayers().length == 4) {
					writer.close();
					writer = null;
				} else {
					boolean allFinished = true;
					for (Car car : world.getCars()) {
						if (car.isTeammate() && !car.isFinishedTrack())
							allFinished = false;
					}
					if (allFinished) {
						writer.close();
						writer = null;
					}
				}
			}
			return;
		}


		if (self.getDurability() < 1e-5) {
			debug(10, "BROKE!");
			MyStaticStorage.clearBlockState(self.getType());
			return;
		}

		Set<List<MyTile>> nextTiles = MyStateCalculator.calculatePath(self, world, game);

		MyMoveOptimalCalculator.calculateOptimalMove(self, world, game, nextTiles, move);

		useBonuses(self, world, game, move);
	}

	private static void init(World world) {
		try {
			assert false;
		} catch (AssertionError error) {
			debug = true;
			try {
				writer = new PrintWriter("debug-log.txt");
			} catch (FileNotFoundException e) {
				System.out.println("Failed to open debug file");
			}
		}
		if(world.getPlayers().length == 2)
			MyMagicNumbers.NEXT_TILES_TO_CALCULATE = 2;
		init = true;
	}

	public static void debug(int lvl, String str) {
		if (debug) {
			if (lvl < debuglvl) {
				System.out.println(str);
			}
			if (writer != null)
				writer.println(str);
		}
	}

	/*
	 * Use bonuses oil and shoot, if it good
	 */
	private static void useBonuses(Car self, World world, Game game, Move move) {
		if (self.getType().equals(CarType.BUGGY)){
			if (MyShooter.isShootBuggy(self, world, game))
				move.setThrowProjectile(true);
		}else{
			if (MyShooter.isShootJeep(self, world, game))
				move.setThrowProjectile(true);
		}
		
		if(MyOilSpiller.isSpillOil(self, world, game))
			move.setSpillOil(true);
	}

}
