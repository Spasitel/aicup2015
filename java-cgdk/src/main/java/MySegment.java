public class MySegment implements MyGeometryObject {
	public final MyVector a, b;
	public final MyTile tile;

	public MySegment(MyVector a, MyVector b, MyTile tile) {
		this.a = a;
		this.b = b;
		this.tile = tile;
	}

	public MySegment(double a, double b, double c, double d, MyTile tile) {
		this(new MyVector(a, b), new MyVector(c, d), tile);
	}

	private static double area(MyVector a, MyVector b, MyVector c) {
		return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
	}


	private static double max(double a, double b) {
		return a > b ? a : b;
	}

	private static double min(double a, double b) {
		return a < b ? a : b;
	}
	
	private static boolean intersect_1_old(double a, double b, double c, double d) {
		if (a > b) {
			double t = a;
			a = b;
			b = t;
		}
		if (c > d) {
			double t = c;
			c = d;
			d = t;
		}
		double max = max(a, c);
		double min = min(b, d);
		return max <= min;
	}
	
	private static boolean intersect_1(double a, double b, double c, double d) {
		double min1;
		double max1;
		double min2;
		double max2;
		
		if (a > b) {
			min1 = b;
			max1 = a;
		}else{
			min1 = a;
			max1 = b;
		}
		
		if (c > d) {
			min2 = d;
			max2 = c;
		}else{
			min2 = c;
			max2 = d;
		}
		boolean result = min1 <= max2 && min2 <= max1;
                
		assert !(result ^ intersect_1_old(a, b, c, d));
		return result;
	}

	private static boolean intersect_segments(MyVector a, MyVector b, MyVector c, MyVector d) {
		return intersect_1(a.x, b.x, c.x, d.x) && intersect_1(a.y, b.y, c.y, d.y) && area(a, b, c) * area(a, b, d) <= 0
				&& area(c, d, a) * area(c, d, b) <= 0;
	}

	@Override
	public String toString() {
		return "[" + a.toString() + "," + b.toString() + "]";
	}

	@Override
	public boolean intersect(MySegment other) {
		return intersect_segments(a, b, other.a, other.b);
	}

}
