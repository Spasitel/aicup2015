public class MyPlayerGeometry {

	private static MyVector[] res = null;

	/**
	 * Counter-clockwise
	 */
	public static MyVector[] playerCorners(MyVector playerCenter, double angle, double playerHeight, double playerWidth) {
		if(res == null){
			res = new MyVector[2];
			res[0] = new MyVector(-playerWidth / 2, -playerHeight / 2);
			res[1] = new MyVector(playerWidth / 2, -playerHeight / 2);
		}
		MyVector[] result = new MyVector[4];
		MyVector rotate1 = res[0].rotate(angle);
		MyVector revers1 = rotate1.multiply(-1);
		MyVector rotate2 = res[1].rotate(angle);
		MyVector revers2 = rotate2.multiply(-1);
		MyVector res1 = rotate1.add(playerCenter);
		MyVector res2 = rotate2.add(playerCenter);
		MyVector res3 = revers1.add(playerCenter);
		MyVector res4 = revers2.add(playerCenter);
		result[0] = res1;
		result[1] = res2;
		result[2] = res3;
		result[3] = res4;
		return result;
	}

}
