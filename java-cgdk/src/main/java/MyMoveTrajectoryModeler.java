import static java.lang.Math.*;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import model.Car;
import model.CarType;
import model.Game;
import model.World;

public class MyMoveTrajectoryModeler {
	private final World world;
	private final Game game;

	private final double forwardAcceleration;
	private final double backwardAcceleration;
	private final int tickIterationsNumber = 10;
	private final double updateFactor = 1.0 / tickIterationsNumber;
	private final double movementAirFrict;
	private final double lwFrict;
	private final double cwFrict;
	private final double angSpdFactor;

	private final double nitroEnginePower;

	public MyMoveTrajectoryModeler(CarType carType, World world, Game game) {
		this.world = world;
		this.game = game;

		if (carType == CarType.BUGGY) {
			this.forwardAcceleration = game.getBuggyEngineForwardPower() / game.getBuggyMass();
			this.backwardAcceleration = game.getBuggyEngineRearPower() / game.getBuggyMass();
		} else {
			this.forwardAcceleration = game.getJeepEngineForwardPower() / game.getJeepMass();
			this.backwardAcceleration = game.getJeepEngineRearPower() / game.getJeepMass();
		}

		this.movementAirFrict = pow(1.0D - game.getCarMovementAirFrictionFactor(), updateFactor);
		this.lwFrict = game.getCarLengthwiseMovementFrictionFactor() * updateFactor;
		this.cwFrict = game.getCarCrosswiseMovementFrictionFactor() * updateFactor;
		this.angSpdFactor = game.getCarAngularSpeedFactor() * updateFactor;
		this.nitroEnginePower = game.getNitroEnginePowerFactor();
	}

	public static class TrajectoryPoint {
		public final MyVector position;
		public final MyVector velocity;
		public final double angle;
		public final double angularSpeed;
		public final double wheelTurn;
		public final double enginePower;
		public final double distanceTraveled;
		public final int ticksModeled;
		public final int nextWaypointId;
		public final int remainingNitroTicks;

		public TrajectoryPoint(MyVector position, MyVector velocity, double angle, double angularSpeed,
				double wheelTurn, double enginePower, double distanceTraveled, int ticksModeled, int nextWaypointId,
				int remainingNitroTicks) {
			this.position = position;
			this.velocity = velocity;
			this.angle = angle;
			this.angularSpeed = angularSpeed;
			this.wheelTurn = wheelTurn;
			this.enginePower = enginePower;
			this.distanceTraveled = distanceTraveled;
			this.ticksModeled = ticksModeled;
			this.nextWaypointId = nextWaypointId;
			this.remainingNitroTicks = remainingNitroTicks;
		}
	}

	public MyMoveModelResult modelTrajectory(Car prevStep, MyDecision decision, int tickLimit, Set<List<MyTile>> paths, int stepInPath, Set<MyBonus> alreadyCollectedBonuses, Set<MyBonus> allBonuses) {

		MyMoveTemporaryResult mModelResult = modelTrajectory(new MyVector(prevStep.getX(), prevStep.getY()), new MyVector(
				prevStep.getSpeedX(), prevStep.getSpeedY()), prevStep.getAngle(), prevStep.getAngularSpeed(),
				prevStep.getEnginePower(), prevStep.getWheelTurn(), prevStep.getWidth(), prevStep.getHeight(),
				decision, tickLimit, null, 1, prevStep.getRemainingNitroTicks(), paths, stepInPath, alreadyCollectedBonuses, allBonuses);

		if (mModelResult == null || mModelResult.isCollision())
			return null;
		else {
			int nitroRemainTicks = prevStep.getRemainingNitroTicks() - tickLimit > 0 ? prevStep
					.getRemainingNitroTicks() - tickLimit : 0;
			Car newState = new Car(prevStep.getId(), prevStep.getMass(), mModelResult.position.x, mModelResult.position.y,
					mModelResult.velocity.x, mModelResult.velocity.y, mModelResult.angle, 0 /* TODO:angularSpeed */,
					prevStep.getWidth(), prevStep.getHeight(), prevStep.getPlayerId(), prevStep.getTeammateIndex(),
					prevStep.isTeammate(), prevStep.getType(), prevStep.getProjectileCount(),
					prevStep.getNitroChargeCount(), prevStep.getOilCanisterCount(),
					prevStep.getRemainingProjectileCooldownTicks(), prevStep.getRemainingNitroCooldownTicks()/* TODO */,
					prevStep.getRemainingOilCooldownTicks()/* TODO */, nitroRemainTicks,
					prevStep.getRemainingOiledTicks()/* TODO */, prevStep.getDurability(), mModelResult.enginePower,
					mModelResult.wheelTurn, prevStep.getNextWaypointIndex(), prevStep.getNextWaypointX(),
					prevStep.getNextWaypointY(), prevStep.isFinishedTrack());
			return new MyMoveModelResult(newState, mModelResult.isEndReach, mModelResult.ticksTraveled, mModelResult.paths, mModelResult.stepInPath, mModelResult.collectedBonuses);
		}
	}

	private MyMoveTemporaryResult modelTrajectory(MyVector pos, MyVector vel, double angle, double angularSpeed,
			double enginePower, double wheelTurn, double width, double height, MyDecision decision, int tickLimit,
			List<TrajectoryPoint> trajectoryPoints, int waypointId, int remainingNitroTicks, Set<List<MyTile>> paths, int stepInPath, Set<MyBonus> alreadyCollected, Set<MyBonus> allBonuses) {

		Set<MyBonus> collectedBonuses = new LinkedHashSet<MyBonus>(alreadyCollected);

		MyVector position = pos;
		MyVector velocity = vel;

		double distanceTraveled = 0.0;

		MyTile curTile = MyTile.getTile(position.x, position.y);
		int newStepInPath = stepInPath;
		Set<List<MyTile>> newPaths = paths;
		
		assert allPathRight(paths, stepInPath, curTile);

		if (remainingNitroTicks > 0)
			enginePower = nitroEnginePower;

		for (int tick = 0; tick < tickLimit; ++tick) {

			double engineAcceleration = enginePower > 0 ? enginePower * forwardAcceleration : enginePower
					* backwardAcceleration;

			if (trajectoryPoints != null && tick > 0 && tick < tickLimit && tick % 10 == 0) {
				trajectoryPoints.add(new TrajectoryPoint(position, velocity, angle, angularSpeed, wheelTurn,
						enginePower, distanceTraveled, tick, waypointId, remainingNitroTicks));
			}

			for (int iter = 0; iter < tickIterationsNumber; ++iter) {

				double cos_angle = cos(angle);
				double sin_angle = sin(angle);

				MyVector lengthwise = new MyVector(cos_angle, sin_angle);
				double dotProduction = lengthwise.dotProduct(velocity);

				{ // position
					MyVector velocityCopy = velocity.multiply(updateFactor);
					position = position.add(velocityCopy);
					distanceTraveled += velocityCopy.getLen();

					if (!decision.isBrake()) {
						MyVector engineAccelVector = lengthwise.multiply(engineAcceleration * updateFactor);
						velocity = velocity.add(engineAccelVector);
					}

					velocity = velocity.multiply(movementAirFrict);

					if (velocity.getLen() > 0) {
						if (decision.isBrake())
							velocity = new MyVector(cos_angle
									* moveNumber(velocity.x * cos_angle + velocity.y * sin_angle, 0, cwFrict)
									+ -sin_angle
									* moveNumber(-velocity.x * sin_angle + velocity.y * cos_angle, 0, cwFrict),
									sin_angle * moveNumber(velocity.x * cos_angle + velocity.y * sin_angle, 0, cwFrict)
											+ cos_angle
											* moveNumber(-velocity.x * sin_angle + velocity.y * cos_angle, 0, cwFrict));
						else
							velocity = new MyVector(cos_angle
									* moveNumber(velocity.x * cos_angle + velocity.y * sin_angle, 0, lwFrict)
									+ -sin_angle
									* moveNumber(-velocity.x * sin_angle + velocity.y * cos_angle, 0, cwFrict),
									sin_angle * moveNumber(velocity.x * cos_angle + velocity.y * sin_angle, 0, lwFrict)
											+ cos_angle
											* moveNumber(-velocity.x * sin_angle + velocity.y * cos_angle, 0, cwFrict));

					}
				}

				{
					double angleDelta = dotProduction * wheelTurn * angSpdFactor;
					angle += angleDelta;
					angle = normalizeAngle(angle);
				}
			}
			MyTile tile = MyTile.getTile(position.x, position.y);
			
			collectedBonuses.addAll(MyBonus.getCollectedBonuses(collectedBonuses, allBonuses, world, game, position.x, position.y, angle));

			// check collision
			MyGeometryObject tileCollision = MyTile.getTileCollision(position, angle, height, width, game, world);
			if (tileCollision != null) {
				MyMoveTemporaryResult mres = new MyMoveTemporaryResult(world, decision, tileCollision, tick, enginePower, wheelTurn,
						angle, position, velocity, distanceTraveled, newPaths, false, newStepInPath, collectedBonuses);
				return mres;
			}

			enginePower = moveNumber(enginePower, decision.getEnginePower(), game.getCarEnginePowerChangePerTick());
			wheelTurn = moveNumber(wheelTurn, decision.getWheelTurn(), game.getCarWheelTurnChangePerTick());

			if (remainingNitroTicks > 0) {
				--remainingNitroTicks;
				enginePower = remainingNitroTicks == 0 ? 1.0 : nitroEnginePower;
			}

			if (!tile.equals(curTile)) {
				newStepInPath++;
				Set<List<MyTile>> tmpPaths = new LinkedHashSet<List<MyTile>>();
				boolean isEndReach = false;
				
				for(List<MyTile> path: newPaths){
					if(tile.equals(path.get(newStepInPath-1))){
						tmpPaths.add(path);
						if(path.size() == newStepInPath){
							isEndReach = true;
						}else{
							assert !isEndReach;
						}
					}
				}
				
				if(tmpPaths.isEmpty())
					return null;
				
				newPaths = tmpPaths;
				
				if(isEndReach){
					MyMoveTemporaryResult mres = new MyMoveTemporaryResult(world, decision, null, tick, enginePower, wheelTurn,
							angle, position, velocity, distanceTraveled,newPaths, true, newStepInPath, collectedBonuses);
					return mres;
				}
				curTile = tile;
			}

		}
		MyMoveTemporaryResult mres = new MyMoveTemporaryResult(world, decision, null, tickLimit, enginePower, wheelTurn, angle,
				position, velocity, distanceTraveled, newPaths, false, newStepInPath, collectedBonuses);
		return mres;
	}

	private boolean allPathRight(Set<List<MyTile>> paths, int stepInPath, MyTile curTile) {
		if(stepInPath >0)
			for(List<MyTile> path: paths){
				if(!path.get(stepInPath-1).equals(curTile))
					return false;
			}
		return true;
	}

	public static double normalizeAngle(double angle) {
		if(angle > Math.PI) {
			angle -= 2 * Math.PI;
		}else if(angle < -Math.PI) {
			angle += 2 * Math.PI;
		}
		return angle;
	}

	public static boolean isLeftRotation(double fromAngle, double toAngle) {
		double angDiff = Math.abs(normalizeAngle(fromAngle - toAngle));
		return Math.abs(normalizeAngle(normalizeAngle(fromAngle - angDiff) - toAngle)) < 1e-7;
	}

	/**
	 * Shift the given number to targetNumber with a step specified by 'shift'.
	 */
	private static double moveNumber(double number, double targetNumber, double shift) {
		if(number < targetNumber){
			double shifted = number + shift;
			if(shifted > targetNumber)
				return targetNumber;
			else
				return shifted;
		}else{
			double shifted = number - shift;
			if(shifted < targetNumber)
				return targetNumber;
			else
				return shifted;
		}
	}
}
