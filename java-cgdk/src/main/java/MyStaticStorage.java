import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Car;
import model.CarType;
import model.World;

public class MyStaticStorage {
	
	private static final Map<MyTile, int[][]> BFSByWaypoints = new HashMap<MyTile, int[][]>();
	private static final Map<CarType, SavedStateFromAStar> savedStates = new HashMap<CarType, MyStaticStorage.SavedStateFromAStar>(2);
	private static final Map<CarType, MyStateCalculator.PathFromTile> pathMap = new HashMap<CarType, MyStateCalculator.PathFromTile>();
	
	private static final Map<CarType, MyMoveExitFromBlock.SavedBlockState> blockState = new HashMap<CarType, MyMoveExitFromBlock.SavedBlockState>(2);
	private static final Map<CarType, Boolean> isBigTimeLimit = new HashMap<CarType, Boolean>(2);

	private static final Map<CarType, SpillOilData> spillOilData = new HashMap<CarType, SpillOilData>(2);
	private static final Map<CarType, MyTile> currentTile = new HashMap<CarType, MyTile>(2);
	private static final Map<CarType, List<HistoryStep>> historyPath = new HashMap<CarType, List<HistoryStep>>(2);

	public static long totalTime = 0;

	public static List<HistoryStep> getHistoryPath(CarType type){
		if(historyPath.get(type) == null)
			historyPath.put(type, new LinkedList<MyStaticStorage.HistoryStep>());
		return historyPath.get(type);
	}
	
	public static void setCurrentTile(CarType type, MyTile tile) {
		currentTile.put(type, tile);
	}
	
	public static MyTile getCurrentTile(CarType type){
		return currentTile.get(type);
	}
	
	public static boolean isPanicMod(World world) {
		if(MyStrategy.ignoreTimeLimit)
			return false;
		int a;
		if(world.getPlayers().length == 2)
			a = 2;
		else
			a = 1;
		return  5000 + 15 * world.getTick() * a - MyStaticStorage.totalTime < 300;
	}

	public static MyStateCalculator.PathFromTile getPath(CarType carType) {
		return pathMap.get(carType);
	}

	public static void setPath(CarType type, MyStateCalculator.PathFromTile path) {
		MyStaticStorage.pathMap.put(type, path);
	}
	
	public static void claerPath(){
		MyStaticStorage.pathMap.clear();
	}

	public static SavedStateFromAStar getSavedState(CarType type) {
		return savedStates.get(type);
	}

	public static void setSavedState(CarType type, SavedStateFromAStar state) {
		savedStates.put(type, state);
	}
	
	public static SpillOilData getSpillOilTile(CarType type) {
		return spillOilData.get(type);
	}

	public static void setSpillOilTile(CarType type, SpillOilData tile) {
		if(tile == null)
			spillOilData.remove(type);
		else
			spillOilData.put(type, tile);
	}
	
	public static int[][] getBFSForWaypoint(MyTile tile){
		return BFSByWaypoints.get(tile);
	}
	
	public static void setBFSForWatpoint(MyTile tile, int[][] BFS){
		assert BFSByWaypoints.get(tile) == null;
		BFSByWaypoints.put(tile, BFS);
	}

	public static boolean isBigTimeLimit(CarType type) {
		if(isBigTimeLimit.get(type) == null)
			isBigTimeLimit.put(type, true);
		return isBigTimeLimit.get(type);
	}

	public static void setBigTimeLimit(CarType type, boolean isBigTimeLimit) {
		MyStaticStorage.isBigTimeLimit.put(type, isBigTimeLimit);
	}


	public static MyMoveExitFromBlock.SavedBlockState getBlockState(CarType type) {
		if(blockState.get(type) == null)
			clearBlockState(type);
		return blockState.get(type);
	}

	public static void clearBlockState(CarType type) {
		blockState.put(type, MyMoveExitFromBlock.SavedBlockState.createEmpty());
	}
	
	public static void setBlockState(CarType type, MyMoveExitFromBlock.SavedBlockState state) {
		blockState.put(type, state);
	}


	/**
	 * Prev decision from AStar
	 */
	public static class SavedStateFromAStar {

		private Car predictedState;
		private final Car endState;
		private final MyMoveDecision prevDecision;
		private final int endTickNumber;
		private final int goalReachedTickNumber;
		private final MyTile startTile;
		private final Set<List<MyTile>> path;
		private Set<MyBonus> collectedBonuses;

		public SavedStateFromAStar(Car predictedState, MyMoveDecision prevDecision, int endTickNumber, int goalReachedTickNumber,
				MyTile startTile, Car endState, Set<List<MyTile>> path, Set<MyBonus> collectedBonuses) {
			super();
			this.predictedState = predictedState;
			this.prevDecision = prevDecision;
			this.endTickNumber = endTickNumber;
			this.goalReachedTickNumber = goalReachedTickNumber;
			this.startTile = startTile;
			this.endState = endState;
			this.path = path;
			assert path.size() == 1;
			this.collectedBonuses = collectedBonuses;
		}

		public MyTile getStartTile() {
			return startTile;
		}

		public Car getPredictedState() {
			return predictedState;
		}

		public void setPredictedState(Car predictedState) {
			this.predictedState = predictedState;
		}

		public MyMoveDecision getPrevDecision() {
			return prevDecision;
		}

		public int getEndTickNumber() {
			return endTickNumber;
		}

		public int getGoalReachedTickNumber() {
			return goalReachedTickNumber;
		}

		public Car getEndState() {
			return endState;
		}

		public Set<List<MyTile>> getPath() {
			return path;
		}

		public Set<MyBonus> getCollectedBonuses() {
			return collectedBonuses;
		}

		public void setCollectedBonuses(Set<MyBonus> collectedBonuses) {
			this.collectedBonuses = collectedBonuses;
		}
	}
	
	public static class HistoryStep{
		public HistoryStep(int tick, MyTile tile, int nextWaypointIndex) {
			super();
			this.tick = tick;
			this.tile = tile;
			this.nextWaypointIndex = nextWaypointIndex;
		}
		public final int tick;
		public final MyTile tile;
		public final int nextWaypointIndex;
	}
	
	public static class SpillOilData{
		public SpillOilData(MyTile fisrtTile, MyTile secondTile, boolean isX, double bestPlace) {
			super();
			this.fisrtTile = fisrtTile;
			this.secondTile = secondTile;
			this.isX = isX;
			this.bestPlace = bestPlace;
		}
		public final MyTile fisrtTile;
		public final MyTile secondTile;
		public boolean isX;
		public double bestPlace;
		
	}

}
